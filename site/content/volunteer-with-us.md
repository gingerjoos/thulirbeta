---
author: nimda321
date: 2011-09-23 12:20:56+00:00
draft: false
title: Volunteer with us
type: page
url: /wp/volunteer-with-us/
---

We welcome you to come and volunteer your time at Thulir for a period ranging from a few weeks to 6 months or more. You can interact with our students and and staff, teach the skills that you have, and learn the skills that you are interested in from our community. We have very basic facilities for room and board in (usually) quiet, peaceful and green surroundings.

Get in touch with us by emailing thulir (at) gmail (dot) com.

