---
author: nimda321
date: 2011-09-30 08:28:52+00:00
draft: false
title: Anuradha and Krishna
type: page
url: /wp/anuradha-and-krishna/
---

### **_
_**




[![](http://www.thulir.org/wp/wp-content/uploads/2008/08/dscn2397-edited.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/dscn2397-edited.jpg)




In the Year 1988, as fresh graduates with a degree in Architecture, and disillusioned with the formal education system, we moved to the village community of Gandhigram in Tamilnadu. Our disillusionment came about with our active participation as students in the National Campaign for Housing Rights. The campaign brought us in contact with a number of grass roots activist organizations and NGOs and gave us a peek into the rural reality, which we had had no clue about as students of Architecture. We realized the severe limitations of the formal educational system and its lack of relevance to problems on the ground. One of us gave up the idea of preparing for higher education and the other discontinued from the Masters course in Architecture midway.




Our work in Gandhigram was a beginning in the long process of re-educating ourselves. We had to learn many skills from scratch [such as masonry!] and learn properties of common but unknown [to us] building materials, such as mud. We also had to motivate and train [and train ourselves in the process] our own team of construction workers who would be interested in alternative and environmentally sustainable building technologies. We received a lot of support from ASTRA in Indian Institute of Science, in learning about alternative technologies.




In Gandigram we also helped set up a women’s group to produce and market items ranging from coir mats to B&W TV sets. This was a formative, crucial phase in our life, when we made friends with like minded people; and decided to get married to each other and  to make an attempt at living & working in rural areas.




Our work with alternative building had attracted some attention and NGOs and even a corporate client approached us for help. We made use of this opportunity to spread some of the ideas we were working with, especially the idea of training local( so called unskilled people ) in building using alternatives.  These technologies are based on the idea of introducing improvements in the use of local materials and generating more local employment. We were surprised that even the corporate client - usually used to the idea of contractors from the metro coming to build their structures, agreed with the idea and pursued it for almost 8 years and helped us to train several groups of village masons n their area.




In 93 we moved to Gudalur, a tribal area in the Nilgiris district of Tamil Nadu. Here we worked with the Adivasi youth. The NGO ACCORD was doing very interesting work with the aim of equipping adivasi youth with varying skills to manage the affairs of their own community. These ranged from skills as community organizers , community health workers, nurses, teachers, masons, plantation managers, hospital administrators, office admin staff. Most of the Youth had not finished schooling. Yet the faith reposed in them by the team , and the quality of training provided by committed professionals, produced spectacular results. The outcome was an Adivasi group of committed individuals who go about their business with dignity and self-respect and display skill levels that evoke the respect of the local non- tribal community that had till now looked down upon them.




Our involvement in the organization gave us a range of experiences from very skill specific[ masons training/ working on the rural housing programme] to the more general one of motivating youth and helping them to grow into self confident adults. Here we did more formalised training sessions, to help the mason trainees get basic engineering skills such as drawing and estimation [ for which we had to start from basics of multiplication and division].




![A Panoramic View Of Thulir](http://thulir.org/about/images/masons.jpg)
_Masons from Gudalur...._




![A Panoramic View Of Thulir](http://thulir.org/about/images/house.jpg)
_....and the house they built_




While this was happening, our children were growing up, we got more interested in Children’s education, and taught them basic math and language at home. We also realized that more than designing and putting up buildings, we really enjoyed teaching rural people. Their keen interest in acquiring knowledge, and the satisfaction of providing knowledge and help one learn a skill was something we found very satisfying. Especially in areas where the quality of Education is appalling [if at all it is available!], and where right education can help a person gain self-confidence and self- respect, we find such interventions more meaningful.




In 2000-2001, we travelled for a year with our children to various projects/ schools, with the idea of getting exposure to working with children. The highlights were teaching English and Math to the tribal health trainees in the sittilingi hospital for 3 months, working with the children in the KFI school in Uttarkashi for 5 weeks and later at the Timbaktu school for 3 months. Our plans had to be modified somewhat due to the earthquake in Bhuj [as Krishna was there on the day of the quake conducting a workshop on stabilized mud block making] . The second half of our year was spent mostly in Kutch working on the rehabilitation of the earthquake affected people.




ACCORD also runs an alternative school, Vidyodaya, which was started for the team’s children and later converted into an adivasi school. Our two sons grew up and started going to Vidyodaya.. From 2001, Anu taught children in this school for two years.




After all these experiences we feel
Ø that children from disadvantaged sections in the rural areas need an education that would help them to gain self respect and help live with dignity. For this they need to acquire skills relevant to the community and the local economy.
Ø That children have a natural curiosity and capacity to learn that needs to be nurtured, giving them space and the right environment.
Ø That children need the company of sympathetic adults who can encourage/ motivate them to acquire skills and knowledge




Regi and Lalitha, who were working with us in Gandhigram as doctors, had moved to Sittilingi in 1993 and set up a community based health programme. We had been making several visits through the years to provide help for the construction of their buildings, and also interacted with their team doing training sessions. We have seen the place and the team grow and heard from them the need for a meaningful intervention in education. So we moved to Sittilingi in June 03.




***********
