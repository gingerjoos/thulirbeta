---
author: nimda321
date: 2011-09-19T09:38:28.000+00:00
title: Alternative Technologies
type: page
url: "/wp/alternatives/"

---
![alternatives](http://www.thulir.org/wp/wp-content/uploads/2011/09/alternatives-31-e1316524410603.jpg)

We at Thulir have been looking at alternatives to the mainstream education system, so that we may be able to cater to the needs of those children who get left out/ left behind.

As part of our learning at Thulir, we have been experimenting with alternative technologies and life styles. Being an Adivasi Village, we thought this is a good place to shed our urban middle- class upbringing/ life style and experiment with alternatives.

### Buildings:

Our house and Thulir campus is built out of local low energy materials like mud, thatch, stone etc. with very minimal use of steel and concrete.

![](http://www.thulir.org/wp/wp-content/uploads/2011/09/alternatives-11-e1317469893587.jpg)

_Rammed earth wall under construction -- sieved local soil mixed with lime and rammed inside a box kept on the wall_.

![thatching](http://www.thulir.org/wp/wp-content/uploads/2011/09/alternatives-21-e1316779140201.jpg)

_Traditional Thatch roof made of grass on coconut leaves and Palmyra wood rafters , under construction_

![house verandah](http://www.thulir.org/wp/wp-content/uploads/2011/09/alternatives-71-e1316779572669.jpg)

![windows](http://www.thulir.org/wp/wp-content/uploads/2011/09/alternatives-4.jpg)

_Windows made from thin cement concrete slabs made at site, to avoid use of wood that will attract Termites_.

### Energy:

The campus is powered mainly by solar PV and we use low power LED lights. For pumping we use a diesel powered pump \[we do not have enough solar PV panels as of now to drive a pump\].

![solar panels](http://www.thulir.org/wp/wp-content/uploads/2011/09/alternatives-91.jpg)

_The Solar panels that provide electricity for lighting, computers, printers and hand drilling machine that we use._

### Solid Wastes:

Kitchen wastes are composted in the land. Cow dung is used to produce Bio-gas. We have built urine separated dry compost toilets, to save water and to get manure for the soil.

![dry toilet](http://www.thulir.org/wp/wp-content/uploads/2011/09/alternatives-61-e1317469695923.jpg)

_Bath water goes to the plant bed with Banana trees_

### Water:

Water is pumped from an open well using diesel powered pump. Grey water \[waste from bath, wash and kitchen\] is used in our Organic Garden. Hence we have minimised use of detergents and chemical cleaning agents and use home made soap, ash/ and herbal products.

We try to minimise use of water by growing millets, pulses and by using drip irrigation system.

![bath area](http://www.thulir.org/wp/wp-content/uploads/2011/09/alternatives-51-e1317469777369.jpg)

_A two pit urine separated Dry Compost Toilet._

### Land:

In the first phase we concentrated on building up the soil as the land was degraded through years of grazing. We dug contour bunds, planted trees, and spread mulch around the trees to conserve moisture and provide humus for the trees. We planted a mix of orchard trees and trees for fodder, fire wood and green manure/ mulch.

Second phase was making small permanent beds for vegetables. We mainly used mulch collected from trees and organic waste from neighbouring farms \[sugarcane leaves, turmenric leaves etc.\].

In the third phase, we got a Cow of native breed, to get cow dung and urine to make various organic fertilizers/ pesticides. We have also started growing pulses and cereals in small quantities.

![alternatives](http://www.thulir.org/wp/wp-content/uploads/2011/09/F_land002.jpg)

![alternatives](http://www.thulir.org/wp/wp-content/uploads/2011/09/F_land021.jpg)

![alternatives](http://www.thulir.org/wp/wp-content/uploads/2011/09/F_land006.jpg)

Test image

![test image alternative](/uploads/k14479003.jpg)

Another test image!

![Test image 2](/uploads/alternatives.jpg)

More test images

![test image 3](/uploads/2020/02/09/alternatives-175x157.jpg)

![test image 4](/uploads/k14479003_2.jpg "test title on test image 4")