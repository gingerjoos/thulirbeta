---
author: thulir
date: 2005-12-18 13:34:00+00:00
draft: false
title: Newsletter December 2005
type: post
url: /wp/2005/12/newsletter-december-2005/
categories:
- Newsletters
---

[![](http://photos1.blogger.com/blogger/8103/1906/320/1.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/1.jpg)
Vediappan goes to Bangalore!

Vediappan, has passed his last exam of class 12. After his exams, got over in September, he felt he needed to look for a job and so has taken a job with a bag making factory in Bangalore. He has been working for the past one month there. While excited at being in a new place and getting new experiences, he is also home sick and exhausted from hard work.

His work shifts are 12 hours long each day for 6 days a week. Being Christmas/ New Year season, he is required to work for 4 hours of overtime above the 12 hr shift. He is paid 1400 rupees, half of which he has to pay his employer for board and lodge.He keeps in contact with Thulir through phone calls. 

There is a common urge amongst youth of his age in Sittilingi to go out and see the world. There is also pressure to get a job and look after themselves financially. We feel this is a challenge for us. We need to work out programmes where in Thulir students can go as exchange students to other learning instituitions for a period of time to gain exposure and confidence.

