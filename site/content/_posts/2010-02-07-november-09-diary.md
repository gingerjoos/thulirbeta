---
author: thulir
date: 2010-02-07 20:19:32+00:00
draft: false
title: 06 November 09 diary
type: post
url: /wp/2010/02/november-09-diary/
categories:
- Life Skills Course Diary
---

**Organic Farming:**

In September we had reported the enthusiastic work that started on a test plot for growing rice using alternative methods. The plot is doing well and the paddy growing healthily.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov001.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov001.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov002.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov002.jpg)

**Cow shed repairs:**
The onset of monsoons meant we had to prepare ourselves against the rains but also hope that we get enough rains to recharge ground water and make it possible to grow vegetables/ crops. The Cow shed needed roof repairs.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov005.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov005.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov006.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov006.jpg)
**
The Motorcycle gets fixed:**

Thulir's 6 year old motor cycle needed servicing and new spares. Our team managed to list out the spares needed, purchased it from Salem and finally fixed it without outside help.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov003.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov003.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov004.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov004.jpg)

**The white LED light fixtures:**

This month more work was done on the assembly. Students were also asked to debug the earlier assemblies which didnt work [there were mistakes made while learning to assemble]. This exercise helped them to understand the circuitry better and also figure where they had gone wrong earlier. The quality of work suddenly improved !

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov008.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov008.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov009.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov009.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov010.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov010.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov011.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov011.jpg)

**The Training Centre construction:**

Meanwhile, the training Centre walls were raised to full height by local masons and the construction stopped for the monsoon. It now awaits a roof structure to come on top.

`[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov0071.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/nov0071.jpg)
