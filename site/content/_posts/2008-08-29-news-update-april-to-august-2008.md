---
author: thulir
date: 2008-08-29 13:58:20+00:00
draft: false
title: News Update April to August 2008
type: post
url: /wp/2008/08/news-update-april-to-august-2008/
categories:
- Newsletters
---

##### [![](http://www.thulir.org/wp/wp-content/uploads/2008/08/pict3328.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/pict3328.jpg)


_Welcome to the News update from Thulir. In this post we review happenings of the past 5 months_


##### Bee keeping update:


The colonies in our Bee boxes are doing well. During end of August, in the 5 boxes we have in Thulir, we were able to get 2.5 litres of honey and the bee colonies seem to be healthy.  [![bee colony in the box at Thulir](http://www.thulir.org/wp/wp-content/uploads/2008/08/dscn2558.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/dscn2558.jpg)


##### Science camp at Thulir


In August we held a weekend camp with Science experiments as the theme.
18 children took part in the Camp that started on a Saturday morning and went on till Sunday evening with the participants staying overnight.
A range of  experiments were carried out:


--testing solubility, conductivity and inflammability of different materials and recording the outcomes
--making of scaled working models of pumps. This is based on the pumps made by Arvind Gupta [[www.arvindguptatoys.com](http://www.arvindguptatoys.com/)]. It was a great hit among the students and everybody had a lot of fun.
--various experiments to understand Bernoulli's principles, air and water pressure, nature of sound and light etc


--observing parts of plants and flowers; transpiration in plants.




[caption id="attachment_48" align="alignnone" width="225" caption=" "][![science experiments 1](http://www.thulir.org/wp/wp-content/uploads/2008/08/a000581.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/a000581.jpg)[/caption]

**Sridhar and Sriranjani **, no strangers to Thulir also visited us during the days of the Camp and participated. Sridhar, being a chemist took a class on molecular structures  using  models. Sriranjani taught the children  new songs to sing.  Sridhar who is a marathon runner and is currently training for a marathon, took the students for a run on Sunday morning and was surprised by their fitness levels.  Senthil  in fact went on a 16 km run the next morning and we are exploring the possibility of his participating in the Kauvery marathon along with Sridhar, Sanjeev and Santhosh, from  Asha Bangalore.

[![](http://www.thulir.org/wp/wp-content/uploads/2008/08/000411.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/000411.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2008/08/000602.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/000602.jpg)


##### Siddharth Dananjay's visit


Siddharth a class 11 student from Indonesia visited Thulir for a week. He interacted with the students and conducted art classes.

[![](http://www.thulir.org/wp/wp-content/uploads/2008/08/a00001.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/a00001.jpg)


##### Govindamma joins us as a Fellow.


Govindammal**,** Aged 17,from Moola Sittilingi village, has recently joined us. She is being trained with Rajammal and Devaki. We hope she would be able to pick up similar skills to Rajammal and Devaki so that we have backup staff next year, in case Rajammal or Devaki decides to leave [marriage/ higher studies etc.] We are also looking for more such suitable trainees and hope to take them along if we get them and have adequate funds.


##### New Students in Thulir


Many children of the 5 to 8 age group have started coming to Thulir this year and we have a new and boisterous bunch of them. Their attendance varies from 15 to 25 each day. Rajammal and Devaki have picked up a lot of skills in being able to engage and guide this group of students. We also have a group of 10 to 14 age group students [mostly boys] about 15 of them attending regularly. Many of them have been attending the previous years too and have suddenly  become senior batch and have opened up showing a lot of self confidence.

[![](http://www.thulir.org/wp/wp-content/uploads/2008/08/00040.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/00040.jpg)


##### The Class 10 exams in March


All the  students who attended Thulir regularly and  wrote the class x public exams in March passed. As this has happened for the 3rd consecutive year, Thulir has earned a reputation among the locals. This has been both good and bad for us. Good, as parents who thought our methods  were  not good as we didnt hit the children and therefore didnt  expect us to succeed, have begun to change their opinions and trust our teaching abilities. Its bad as a lot of children who have never been part of Thulir sessions now come to Thulir just before the exams and want to be coached. A couple of them even came the day before the exam as they thought it would bring them good luck and help them pass! One of the parents wanted to take her daughter off the Govt School  10th class and send her to Thulir full time for coaching!

We have now made it clear to the Villagers that only those children who come regularly to Thulir and have attended at least for 2 years before they appear for their exams would be helped.  We have had to do this as for most children even their basic reading/ writing skill levels are poor and so it is an uphill task to get them to a level of being able to write their 10th exams.


##### Going Back to school / higher studies!


There is a big wave this year among the youngsters in the villages here to continue schooling and to go out for higher studies. While this used to happen with just a few students till now every year, this year most students who passed their 10th [even the ones who finished after many supplementaries] have joined class 11 in schools outside the area, many of them joining hostels to do so. All these years  most young people used to migrate outside to work [mainly in the garment industry, but this year it has been for studying.

While this looks like a good thing as  young people get a few more years of  student life away from grueling work, the quality of education they are pursuing is poor. They often go to Govt run schools [with hardly any teachers] or to dubious private institutions that have mushroomed all around. Once the students go outside their village for higher studies, the expenses incurred on education goes up steeply and in the village there is no understanding of what quality in education means and what are the likely benefits of higher education [save for a vague promise of Govt. jobs!].

There is aggressive promotion by private educational institutions and parents and students are being lured by promises of impossible dreams into spending large sums of money. While on the one hand we want Thulir children to acquire good academic skills and do well, on the other hand we have had to try to talk to them and their parents about the dangers of the present trends. But this has been an uphill task for us and a source for great frustration.

[![](http://www.thulir.org/wp/wp-content/uploads/2008/08/pict3583.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/pict3583.jpg)


##### The Kitchen


Our temporary Kitchen facility has been serving us well these past 8 months. We have had a steady stream of visitors and volunteers. The 2 part time cooks Rajkumari and Jyothi have been managing cooking the food. As they are both locals and used to cooking  only the traditional diet, we have had to train them in cooking different kinds of food for us and the outsider visitors. This has also taken much effort and Mrs Vanaja Ravindran Volunteered and taught them many new dishes. We have also incorporated many of the traditional healthy millet items to introduce it to our visitors and in an attempt at trying to preserve the tradition. This we feel is important as the food habits in the villages are rapidly changing towards unhealthy polished rice diets bereft of proteins and vitamins.


##### New building construction


We started the construction of a proper Kitchen, store room, guest room and dining hall in May. As we usually do, this building too is built using local materials to a large extent and local labour. This involves training local youth in building using alternative materials and methods of construction, as local skilled labour is reluctant to build in this way. We now have a steady group working on this building and learning many of the techniques. We are also planning more intensive training sessions for Annamalai, Murugan and Ezhumalai, so that their skill levels improve further. This activity has been very time consuming and we have managed by starting the work day for this activity from 6 am.

[![](http://www.thulir.org/wp/wp-content/uploads/2008/08/00003.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/00003.jpg)


##### Summer break in May


Thulir was closed to students in May as were doing our annual repair work / maintenance work of the buildings. We also took a break for 2 weeks and were away meeting family. The senior students looked after the campus and the  maintenance work while we were away.


##### Visitors in March


This year we had 2 student Volunteers from the Mahindra World College, Pune visitng us. Niwaeli and Eneli from Tanzania and Zambia spent a week at Thulir and interacted well with our students. Learning about Africa and life in tribal villages there was very very interesting. The similarities in that exists between Sittilingi and villages there was surprising. The traditional food, and housing were quite similar. Learning all this our senior students got along very well them.They loved to eat the traditional millet balls in Sittilingi as they had been missing it in Pune. Their hair was a matter of surprise and curiosity for everybody here. One day when they went with Rajamma and Devaki to the nearby village weekly market, all the shopkeepers and shoppers crowded around them with good natured curiosity and all the girls returned back with huge grins in their faces!

Vinu our visiting student from Kanavu, Wayanad went back to Kerala in the summer as his family was building a new house and needed extra hands to help. He was to return in July, but unfortunately he injured himself while practising Kalari [a Kerala martial art] and so his coming back has been postponed.


##### Books from Mrs Aruna Sethupathy


We received beautiful books in excellent condition from  Mrs Aruna Sethupathy. She has painstakingly collected these books from various sources and sorted them to make a bundle of  books appropriate for Thulir. We would like to thank Mrs. Aruna Sethupathy for this magnificent gift.


##### [![](http://www.thulir.org/wp/wp-content/uploads/2008/08/pict3649.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2008/08/pict3649.jpg)




##### ***************************************************
