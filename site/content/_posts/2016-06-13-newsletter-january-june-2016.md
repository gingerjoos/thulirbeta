---
author: nimda321
date: 2016-06-13 14:37:46+00:00
draft: false
title: Newsletter January-June 2016
type: post
url: /wp/2016/06/newsletter-january-june-2016/
categories:
- Newsletters
---

There is fresh life all around us as we write this! Fresh grass, tender leaves, new flowers, new sprouts … Yes, a few good showers in the last 10 days have ended the blistering heat of the hottest summer here for ages, bringing great relief. All of us are now looking forward to the start of the academic year!


### New Thulir Road


When Thulir started in 2004, there was hardly a proper road connecting it to the village. The mud path from the village ran over a stream bed that would fill up with water whenever it rained. Children would stop coming to Thulir whenever this happened. The road used to be so slushy that many a pair of slippers have been lost in the clay.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160522_120951132.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160522_120951132.jpg)
A bridge was finally constructed across the stream some years ago. But now from February this year we can boast of a brand new tar road!

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160522_120442183.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160522_120442183.jpg)


### Land At Last


After two years of intense searching, land for the new school has been identified and was bought this March.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160606_171521735.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160606_171521735.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160606_171525444.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160606_171525444.jpg)

This 3 acre piece of land is more central in the valley, about 1.5 km from the present Thulir campus. It is also easily accessible from Velanur, Kalaiyankottai, Nammankadu, Sittilingi, Moolasittilingi and Malaithangi. Yet, it is also far enough from any government school to satisfy all the requirements.

The 2 acre land which Thulir had earlier bought for the resource centre was found unsuitable for the school. It had no scope for expansion and was very close to the Sittilingi village and the existing government primary school. This land has now been transferred to the Tribal Health Initiative for its Organic Farming marketing activities.

We have now fenced the new land. In the coming month we will dig the well and then start the construction.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160606_171517946.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160606_171517946.jpg)

The process of designing the school is on, with inputs being taken from both teachers and students.


### New Teachers


It was felt that the school needed a few more teachers with degrees in education. Once again, news of this spread through word of mouth to all the villages. Ten local youngsters with B. Ed and DT. Ed degrees applied. For the first time in the history of Thulir we had to conduct an exam and an interview in order to select people![![](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160613_122828544.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160613_122828544.jpg)
Jeyachitra and Sinthamani from Naikuthi and Rettakuttai villages respectively have joined the Thulir Team from April 1st. They seem to have bonded well with the rest of the team already.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160613_123029448_HDR.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/IMG_20160613_123029448_HDR.jpg)


### Sports Day for the Government School


Regular evening classes for the government school children were put on hold this year as the Thulir team was preoccupied with the starting of the primary school. However, some children came in the morning to train for the 10k, 5k Runs that we participated in this year. We also conducted a Sports Day in February for them.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/P1000430.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/P1000430.jpg)
Though there were no formal announcements of this event, word had spread in just two days and a total of 104 children turned up to participate. The sweltering heat of the day could not curb the childrens' enthusiasm; they all declared that they wanted the events to continue and never stop!

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/P1000414.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/P1000414.jpg)

As usual, there were prizes for all the participants and not just the winners.


### Annual Day – April 16th


The Annual Day at the end of the academic year was an opportunity for parents, teachers and children to get together and share the achievements and trials of the year. The children, many of whom had been completely shy—refusing to come out from behind their mothers' sarees—when they joined a year ago now went up confidently onto the stage to sing and dance and act. After a while, they all left as a group to quietly engage in drawing and painting, leaving the adults to have a one hour meeting. Finally each one of the children took their parents to their class to show them their files and all their work for the year. At the end of the day, the parents all appeared to be very happy with what they had seen.


### Participation in the Freedom Inclusive Summer Camp for Children: May 2nd-May 7th


Sakthivel, Ravi, Mohan, Perumal,Satish Raghu and Karthik participated in the Freedom Summer Camp for Children organised by The Runners High. The objective of the camp was to bring together children from completely diverse backgrounds in order to help create awareness and respect for each other.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/20160505_074053.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/20160505_074053.jpg)
[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/20160505_124457.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/20160505_124457.jpg)

Children from SSK, Shrishti Special Academy, Snehagram, Thulir and Ananya participated. The children stayed in the Spastic Society and Pegasus campuses and engaged in various Art, Craft and Theatre activities together.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/20160505_151249.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/20160505_151249.jpg)


### Participation in the Asha Summer Teacher Training Session May 27th-May 31st


Rajammal participated in a teacher training camp organised by Asha Chennai at IIT Madras campus. Her confidence and skills have been given a definite boost.


### [![](http://www.thulir.org/wp/wp-content/uploads/2016/06/20160531_091504-e1465811397222.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/20160531_091504.jpg)




### Participation in Runs


Senthil and Ravi ran the Bangalore 10 K run on May 16th  to raise funds for the Tribal Health Initiative.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/06/20160605_072316.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/06/20160605_072316.jpg)
Sakthivel, Mohan, Raghu, Karthi and Perumal participated in the Anandayana Run organised by Runner's High on June 6th. This was an opportunity for them to once again bond with the students of Ananya  School, Spastic Society and the runners from Runner's High. It was good for them to renew contacts with old friends and make new friends.


### Visitors/ Volunteers


Julian and Miriam from Germany conducted English language games with the teachers and children.

Old friends from Asha, Sridhar Desikan (with his daughter Janavi) and Balaji visited. It was good to have them here again.

The headmistress and teachers from Vidya Peetam, Salem, also visited.

Thank you for being a part of all our efforts so far. Hope you would stay with us as we now raise funds to build the new school and help create a meaningful learning community of children and adults together.
