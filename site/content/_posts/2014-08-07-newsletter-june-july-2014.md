---
author: nimda321
date: 2014-08-07 04:38:27+00:00
draft: false
title: Newsletter June - July 2014
type: post
url: /wp/2014/08/newsletter-june-july-2014/
categories:
- Newsletters
---

**Newsletter - June - July 2014.**

[![](http://www.thulir.org/wp/wp-content/uploads/2014/08/Insects.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2014/08/Insects.jpg)

**Rain, rain Everywhere but not a drop here**

Waiting and waiting for rain, in vain, has become one of our major activities here, and water, our most valuable resource! We can completely empathize with the millions of farmers in our country. This summer has been the most severe one we have experienced in the last 10 years. The temperature soared above 40 degrees Celsius most days. The water level in our well kept reducing and from March, we have been managing with one or two feet of water in our well. For the first time, we had to close Thulir down for a whole month in May for students and visitors.

Senthil, Sakthivel, Jayabal, Parameshwaram and Perumal worked in the sweltering heat deepening the well during the vacation. A few showers towards the end of May lifted our hopes up. The plants, grasses and trees sighed with relief and put out tentative new green leaves and buds. We immediately ploughed our land and planted Saamai (little millet) and kambu (pearl millet/bajra) - but alas, the parched land soaked up the moisture very soon and there were no more showers after that.

In June, the Sittilingi Panchayat came forward to help and dug a borewell here. But even that had no water! Thulir re opened the same month and we kept managing with little or no water. When suddenly on 3rd July, there was not a drop of water left in the well!

Now desperate measures were needed. We were wondering what to do when Gunashekharan,a farmer who lives half a km away came forward on his own to help. His well had water and he offered to fill our well with some water. Senthil, Sakthivel, Mohan, Perumal, Ravi, Nikhil and Krishna worked for two days laying out pipes from Shekhar's well to ours along and across the Panchayat road. The distance between the wells was measured to be around 400 odd feet. Some pipes were borrowed from Sakthi, another farmer we knew, and the rest from Shekhar himself.

(Technical aside - We had a total of twenty pipes, each measuring 20 feet in length and 2 inches in diameter. The pipes used were the semi-flexible hard plastic kinds with a connector and a clamp on one end and a receiver on the other. The joints were the weakest part of the 400 foot pipe snake, leaking and spraying thin jets of water into the air). With this we filled our well with water in a matter of 8 hours (and watered all the sugarcane and arecanut fields that the pipe snaked through because of the leaky joints). The most surprising part was, there was no money transaction. This, we thought, could only happen in Sittilingi!

[caption id="attachment_2006" align="alignleft" width="320"][![](http://www.thulir.org/wp/wp-content/uploads/2014/08/Pipe-july14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2014/08/Pipe-july14.jpg) Many 20 foot pipes connected together running through the thulir grounds[/caption]

[caption id="attachment_2010" align="alignleft" width="320"][![](http://www.thulir.org/wp/wp-content/uploads/2014/08/Water-july14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2014/08/Water-july14.jpg) Life flows though a pipe..[/caption]































We are managing with that water now, and still waiting for the monsoon … It looks like the well is losing on an average 3 inches of water everyday, even on days we don't pump out the water. From the time we have filled it, the water level has dropped 8 feet. 8 feet since the third of July…

[caption id="attachment_2000" align="alignleft" width="320"][![](http://www.thulir.org/wp/wp-content/uploads/2014/08/dry-hill-july14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2014/08/dry-hill-july14.jpg) _Contrasting colours_ - A well watered mango orchard and arecanut farm looking lush against a drying up bamboo forest and hillslope[/caption]

















**What is work? What is play?**

****A younger group of boys (all aged between 14 and 17) - Thirupathi, Udayamurugan, Solomonraj, Karthik, Annamalai, Charlie and Hari were here the whole summer in the punishing heat watching the well deepening and helping out now and then. We urged them to go home and have fun - it was summer holidays after all, but they didn't. This was FUN for them and one could see that. Does this too happen only in rural/tribal areas or is it everywhere? Teenagers wanting to do physical work in the hot sun instead of sitting at home and watching TV? Maybe the fact that they were not forced to work made it fun for them? They definitely wanted to ape the young adults and do what they do!

**Lakshmi**

[caption id="attachment_2002" align="alignleft" width="320"][![](http://www.thulir.org/wp/wp-content/uploads/2014/08/Lakshmi-july14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2014/08/Lakshmi-july14.jpg) Lakshmi with the newborn calf.[/caption]















A quiet, unassuming member of our family, our cow Lakshmi had patiently put up with all our inexperience, clumsiness and eccentricities passed away suddenly in May. She had given birth to a female calf just on May 1st without any fuss or whimper. The mother and calf were doing well when this happened. Fortunately for us, Ambika's cow had just lost her calf. So we have now left our calf with that cow. The calf is growing well now under Kannagi's care.



**Marks! Marks! MARKS !**

A major change in our valley the last 10 years has been that the 'marks fever' which has gripped the rest of Tamil Nadu has caught us too! The frenzy for scoring marks at the cost of everything - even education -  has entered Sittilingi too, sadly.

The government school here has claimed 100 percent pass in the class X board exam. One of the Thulir students had scored 464 out of 500 in this exam. What astonished us was that his parents were far from happy. They were angry with him for not scoring higher marks than the boy studying in Kottapatti(the neighboring village). The irony of the situation was that the parents of the boy in Kottapatti who had scored 484 out of 500 were unhappy that he didn't score as high as a boy in the next town!

Where is all this leading us? What are we doing to our children?

Admission to professional colleges in Tamil Nadu is based on marks scored in the class XII examinations. Ironically (more irony), this move was done 6 to 7 years ago to put rural students at par with urban kids! Many schools which only coach students to score high mushroomed all over TN. Unfortunately, we are just 100 to 150 km away from where these schools are located, a "lucky" place called Rasipuram. A few such schools have come up in towns 50-60 kms away too.



**Victims of success stories**

Much later, we found out that the hundred percent pass in the government school was possible only because the teachers have slowly and subliminally (and later on, quite openly too we heard) persuaded some students to drop out by telling them persistently that they were not good, they can NEVER study, they were less intelligent compared to their classmates, etc.

If they are rejected by even the government school, where will the so called "backward students go? Whose fault is it that they are "behind" in studies? How can any teacher dismiss a student as "good for nothing" ?

We found two of those students, Mohan and Perumal, and they are now full time at Thulir. They are still very diffident and hesitate to even speak. But already, after just 3 weeks, one can see a sparkle in their eye and the beginnings of joy in their smiles! They are definitely interested and will soon bloom.



**Weekend camps **

[caption id="attachment_2007" align="alignleft" width="320"][![](http://www.thulir.org/wp/wp-content/uploads/2014/08/Saturday-july14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2014/08/Saturday-july14.jpg) "Roller Coaster" - this paper will be rolled up into a cylinder. The story rolls on, one page at a time. The paper is loaded into an old shoe box with two firm cylinders on each end. The movement is similar to a cassette.[/caption]





















Since Saturdays are usually  an uninterrupted day with the kids, we have decided to make the most of them and do more interesting things to create/sustain interest in activities other than academics. We have had treks into the forest, visits to a nearby waterfall, where we got to observe many life forms in the wild, clay modelling , craft Workshop, sports day and film screenings. There was a rather beautiful film called "Animals are Beautiful People" which is a hilarious, yet informative documentary on the wildlife of the kalahari desert, and an ecosystem around the Nile.



**Sports day- No Water but full Energy!**

An important part of any child's development is the often overlooked, much under played, sports. It's clear to anyone who observes children play that they learn much from their play times while enjoying it. Relating to each other, team work, handling success and failure etc. are all learnt during play. I myself have observed that whenever I play and exert myself, finishing up sweaty and tired, I am always in a more cheerful mood, and with a mind that is cleared of the day's accumulated dust.

[caption id="attachment_2008" align="alignleft" width="320"][![](http://www.thulir.org/wp/wp-content/uploads/2014/08/Sportsday-july14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2014/08/Sportsday-july14.jpg) Thulir favorite - Kabaddi![/caption]

















On a Saturday, we had a sports day for the children with a lot of team games. These are days when we get to see how much energy the children really have. We had just one pot of water that day for drinking and there were 60 children playing the whole day!  But we could only manage because we were in Sittilingi! No one complained. Children brought water bottles and food  from home, someone cheerfully brought a pot of water from their home in the afternoon and we all had fun!



**Trek to chettikuttai**

[![](http://www.thulir.org/wp/wp-content/uploads/2014/08/Trek-july14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2014/08/Trek-july14.jpg)



Chettikuttai is a small hamlet,  about 6 km away, in the middle of the forest off the Sittilingi - Kottapatti forest road. We trekked there along with 40 children one Saturday. Time and distance passed unnoticed while observing the various trees, birds, picking wild berries of various kinds, exchanging stories and eating the food we carried! On our way back we had a sudden crisis. Siva, a ten year old, kept complaining that he was tired. This is very unusual among tribal kids - they are never tired during physical activities! So some of us lingered slowly behind with him telling stories and singing songs to encourage him. Suddenly Siva collapsed and had a siezure! Luckily we were on the main road by then. Some bikes stopped and gave some of us a lift to the Tribal hospital with him. Within a short while Dr. Regi was reassuring us after examining him. Siva has completely recovered now.



**Discussions on thulir**

We feel it is important to keep discussing, from time to time, the philosophy and ethics of thulir with each other. Along with that we also have had meetings where we got together to discuss stories, exchange notes and share each other's experience of being with children, opinions on what we feel is working and what isn't, problems we are facing, new ideas to take things forward, taking another look at why we are the way we are at thulir (with respect to following an alternative model of education), how we can all be better, more effective teachers, the need for organizing our time properly, trying out the teaching methods on ourselves before trying them on students so we can better predict problems and the experience of learning a concept from another angle (an alternate angle!) and more such themes.

These meetings have always been very helpful in understanding each other and the pitfalls of our own teaching methods. Such meetings, I feel, are valuable and bring all of us working here closer together and will continue to happen regularly.



**Ananda Yana Run, Bangalore**

[caption id="attachment_2004" align="alignleft" width="320"][![](http://www.thulir.org/wp/wp-content/uploads/2014/08/marajuly14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2014/08/marajuly14.jpg) (L-R) Solomon Raj,  Charlie, Karthik, Hari, Annamalai, Sakthivel, Udayamurugan.[/caption]

















The running group consisting of seven boys this time went to Bangalore in June accompanied by Senthil and Sakthivel for the Ananda Yana(in kannada, that means "Journey of Joy") Marathon. They were all registered for the 10 km run. Staying at Ananya school, an alternative school in Bangalore,  they were very close to the starting point of the run. The run is organized by Runner's High, a group who want to bring the concept of fitness to people across different socio-economic backgrounds.

Since it was a weekend, boys could also visit Lalbagh, K.R.Market, and S.P.Road to do some of their shopping and also experience what a city is like. They all loved the experience and want to go back very soon! The lights, the cars, bikes, roads, tall buildings, colourful shops, the wares they sell.. One can imagine a child who has been in a secluded village for all his life suddenly in the bustle of K.R. Market, looking at the crowd and things on display, people busily moving about their work, traffic congestion.. Maybe they wouldn't like to live there, but visiting such places is surely fun for them!



**Balaji Was Here**

Our good friend from Pondicherry was staying here for close to a month. He took classes for the children in the evenings and helped out with the accounts and the computer department. He also started the children off on "Scratch" , a beginners level programming software with a graphical user interface designed for children by Ubuntu. Although we had planned a cycle trip with him and were almost ready to leave, we had to "postpone" it. The time for cycling was not right i guess, but we soon hope to have a trip with him anyway. With his loud voice and singing of old Tamil songs, and frequently voiced political opinions, we would all know where Balaji was.

Anu's father has been diagnosed with advanced cancer and has been taking treatment at Vellore. So Anu has had to be away for atleast a week each month since December. So it was good for all of us to have Balaji here at those times.



**A new teacher arrives! (with a B.Ed.)**

Ravi, a B.Ed. graduate was working in a village nearby at a government school for four months without pay and was tired of it. He wanted out and we were also looking for another teacher for our evening sessions. It was good timing, and soon, Ravi joined Thulir. He has been taking classes in the evening. His report of thulir would be interesting to read, we imagine, as he is finding everything at thulir very different from the government school environment that he is used to. After having a few meetings together, he has voiced out his opinions and issues of working here, and we have all been giving each other little ideas to overcome the problems. Ravi was present for the well to well water transfer episode, some wood work (we were making tangrams) and chopping firewood, so he now knows what it means to be a teacher in thulir!



**Poornima's workshop**

Poornima from Marudam school, Tiruvannamalai graced us with her presence at Thulir! She came here to conduct a weekend long workshop for the teachers on the Montessori Method of teaching young ones. One of the best things to happen to thulir in a long time, her workshop managed to blend having fun and learning, philosophy and practice, singing and writing, dancing and drawing, laughing and introspection, dialogue and monologue, learning and un-learning! So it was a very good event for all in terms of new methods of teaching, using the resources we have more effectively, like our library and all the plants around us, and having a different perspective on education and children itself. We divided the time we had into looking at different age groups and how they learn, but at the end of it, we felt that we did not have enough time with her to go through all. We all (i think) emerged from the weekend as slightly changed people in our ways and outlook towards educating our next generation.

[caption id="attachment_2036" align="aligncenter" width="320"][![](http://www.thulir.org/wp/wp-content/uploads/2014/08/Bulbulbajrajuly14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2014/08/Bulbulbajrajuly14.jpg) Red whiskered bulbul on a bajra plant[/caption]


