---
author: thulir
date: 2010-09-28 23:37:19+00:00
draft: false
title: Newsletter -- June -Sept, 2010
type: post
url: /wp/2010/09/newsletter/
categories:
- Newsletters
---

[![pookolam](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_6926.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_6926.jpg)


## [**Reflections on the past 6 years!!**](http://www.thulir.org/wp/?p=778)


On April 14th, the Tamil New year day, in 2004, we inaugurated the Thulir Learning Centre at Sittilingi. It is more than 6 years since then, and we thought it is a good time now to reflect back and share our journey so far. In the initial years, there were many periods of frustration. We started our work with the 10-14 age group of mainly school going children. We started with after school classes in the evenings and also during the day on weekends. The appalling level of academic skills among the children seemed quite insurmountable. ......[READ FULL ARTICLE](http://www.thulir.org/wp/?p=778)


## [**Evaluation of Thulir by Students**](http://www.thulir.org/wp/?p=783)


We recently had a round of evaluations of Thulir both by the smaller children and the young adults.During the first round of evaluation the following people were pesent - Teenagers group of 10 students [the present senior batch], Perumal, Rajammal, Devagi, Senthil, Ravi, Anu and Krishna.The object was mainly to get a feedback of students' opinions, confidence, skills gained etc.This was a session of group discussions interspersed with a few questions which everybody answered anonymously in writing..... [READ FULL ARTICLE](http://www.thulir.org/wp/?p=783)

[![growing team](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_6670.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_6670.jpg)


## The Thulir team is growing!!


A very significant development is that our Thulir team is growing. Ravi Chandran, who has worked with Tribal Health Initiative since its inception in 1992 and who had taken a break from his work there has joined Thulir to teach. He is a very good teacher. He is a Mechanical Engineer by training who worked in Admin at THI and so has a variety of experience and skill to offer. Already in the 3 months since he has been with us, he has made a big difference in the programme of the senior children. He has spent considerable time counselling students who have finished class 10 and 12 about higher studies opportunities – colleges, courses, helping fill applications, available scholarships, etc. He has also been coaching students preparing for re-exams for class 10 and 12 and plans to continue this activity as part of Thulir's schedule this year, since there is a demand for this from the students. He takes regular classes in general studies [news analysis], puzzles and quizzes, Tamil language skills, and helps in Workshop projects that our senior batch of BT Course students are doing. Sanjeev and Anita (Asha-Bangalore volunteers) have also shifted to Sittilingi for 6 months. They have been frequently volunteering at Thulir for the past 3 years and you may remember about their sessions on electronics, puzzles and singing. We are excited to have them volunteering here and teaching students at Thulir.




## Purchase of Land for Thulir Campus


After a few years of looking around to purchase suitable land for Thulir's campus,  we have got lucky and have now  purchased 2 acres of land very close to the present place, closer to the village. We gradually plan to put up buildings and eventually have a campus of our own.we have to raise the money [about Rupees 7 lakhs] that we spent for the land purchase and raise additionally for the buildings in the future.


## The Evening Batch Children


The evening batch children have been coming regularly and now we have more girls in this batch than boys. They have been divided into groups and we now have regular 4 to 5 parallel sessions. We also have regular preparatory sessions for the teachers where topics and methods of teaching are discussed.

This month these children were taken on a hike to the nearby forest. Here are some pictures from the hike.

[![Hike 1](http://www.thulir.org/wp/wp-content/uploads/2010/09/screenshot-100_6966-mov-1.png)
](http://www.thulir.org/wp/wp-content/uploads/2010/09/screenshot-100_6966-mov-1.png)
[![Hike 1](http://www.thulir.org/wp/wp-content/uploads/2010/09/screenshot-100_6980-mov-1.png)
](http://www.thulir.org/wp/wp-content/uploads/2010/09/screenshot-100_6980-mov-1.png)


## The New Batch for Basic Technology Course joins


A new batch of 2 girls and 4 boys have joined the Basic Technology course this year. This is the first time we will be having a senior and a junior batch of the course studying together in Thulir. Of the Senior batch, Vijay Kumar has left the course to try to become an insurance agent. Srikant, Siva and Velu have returned to Gudalur to continue training and working at the Adivasi instituitons in Gudalur.

[![welding](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_6627.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_6627.jpg)
[![srikant siva velu](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_6824.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_6824.jpg)


## Wiring at the Workshop building.


The New Workshop building is complete and the Electrical wiring work had to be taken up. The senior batch along with Perumal took on this job and have successfully completed a complicated wiring job. There is a 3 phase supply and separate circuits for lighter lighting loads and for individual machinery.Along with the actual work, detailed stock maintenance while working, making estimates of material requirement for each stage of work, and costs calculations were taken up.

[![wiring](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_7080.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_7080.jpg)


## Welding training.


As the workshop building is ready three of our senior students learnt steel fabrication work which involved deign and drawing of a Farm Gate, cutting and welding steel, and finally painting and erection at site. Detailed material consumption and labour input figures were maintained and a costing exercise taken up.


[![welding 1](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_6628.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/09/100_6628.jpg)******************************
