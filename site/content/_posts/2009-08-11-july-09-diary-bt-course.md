---
author: thulir
date: 2009-08-11 11:19:10+00:00
draft: false
title: 02 July 09
type: post
url: /wp/2009/08/july-09-diary-bt-course/
categories:
- Life Skills Course Diary
---

**July 09**

**Balaji's visit:**

Balaji, a volunteer with Asha visited us and spent a few days. He had come to Sitttilinig ona a bicyle and this generated a lot of interest in biking and bicycles. Balaji explained how gear bicyles work and showed our students how to use the gears.

[![july031](http://www.thulir.org/wp/wp-content/uploads/2009/08/july0311.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july0311.jpg)
**Building Construction Part 1 – foundation of Cycle shed**

With the increasing numbers of students coming to Thulir in their bicycles and also the two wheelers of Thulir itself, we feel the need for a proper parking space. So it was decided to build a cycle shed. Here are the pictures of the foundation being done.

[![july030](http://www.thulir.org/wp/wp-content/uploads/2009/08/july030.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july030.jpg)

[![july028](http://www.thulir.org/wp/wp-content/uploads/2009/08/july028.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july028.jpg)

**Session on Welding Basics**

We have recently recieved a donation of asingle phase welding mahine.Ramasubramanian from Bangalore visited us and he showed the basics of welding. This was such an interesting skill, our students couldnt stop welding and the session ended after a lot of reluctance!We are in the preocess of identifying a welding instructor locally to help improve our welding skills.

[![july017](http://www.thulir.org/wp/wp-content/uploads/2009/08/july017.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july017.jpg)

[![july016](http://www.thulir.org/wp/wp-content/uploads/2009/08/july016.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july016.jpg)

[![july026](http://www.thulir.org/wp/wp-content/uploads/2009/08/july026.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july026.jpg)

**Building Construction Part 2 -- making cement stabilized Adobe bricks**

We have seen Dr Yogananda's work on stabilized adobe in his office in Bangalore and thougth it is an interesting technique to try out. A mix of soil, sand , cement [added as a stabilizer - usually from 5 % to 10% by weight of the mix] and water is mixed and pugged before being shaped in a mould by hand and then cured by adding water for 3 weeks . The following pictures show the process.

[![july025](http://www.thulir.org/wp/wp-content/uploads/2009/08/july025.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july025.jpg)

[![july023](http://www.thulir.org/wp/wp-content/uploads/2009/08/july023.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july023.jpg)

[![july021](http://www.thulir.org/wp/wp-content/uploads/2009/08/july021.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july021.jpg)

[![july020](http://www.thulir.org/wp/wp-content/uploads/2009/08/july020.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july020.jpg)

[![july019](http://www.thulir.org/wp/wp-content/uploads/2009/08/july019.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july019.jpg)

[![july018](http://www.thulir.org/wp/wp-content/uploads/2009/08/july018.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july018.jpg)

**Building Construction Part 3 -- Learning basics of  Brick Masonry**

Now that brick making has been learnt, the next step is to learn to build a wall with bricks, so a few sessions on understanding basics of masonry were held. For this burnt bricks and a simple sand mortar [just sand and water and no soil or cement, so that it is easy to take the bricks apart after the session], were used.

[![july010](http://www.thulir.org/wp/wp-content/uploads/2009/08/july010.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july010.jpg)

[![july011](http://www.thulir.org/wp/wp-content/uploads/2009/08/july011.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july011.jpg)

[![july013](http://www.thulir.org/wp/wp-content/uploads/2009/08/july013.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july013.jpg)

[![july014](http://www.thulir.org/wp/wp-content/uploads/2009/08/july014.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july014.jpg)

**Building Construction Part 4  Building the wall of cycle shed**

Once the basics were clear, it was time to start applying what was learnt.... so went back to the cycle shed construction and after doing the fouldation out of stone and mud, decided to buid the wall. As a first step we thougth we will learn one more technique of using mud for wall and this is the Rammed Earth Wall. We made a mix of Soil, sand, cement and slaked lime [burnt lime stone added to water]; and the resulting mix was compacted inside a box kept on the wall. This rammed portion can been seen on the lower part of the wall in the pics below.

[![july006](http://www.thulir.org/wp/wp-content/uploads/2009/08/july006.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july006.jpg)

[![july007](http://www.thulir.org/wp/wp-content/uploads/2009/08/july007.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july007.jpg)

[![july008](http://www.thulir.org/wp/wp-content/uploads/2009/08/july008.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july008.jpg)

[![july009](http://www.thulir.org/wp/wp-content/uploads/2009/08/july009.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july009.jpg)

**Sanjeev and Vinod teach electronics!!**

Sanjeev and Vinod, both electronics engineers, visited us and took a couple of sessions on electronics. Sanjeev has been developing a curriculum for teaching electronics in schools – more at his blog http://electronics4kids.wordpress.com/.
Vinod has written a blog on his visit http://vinning.blogspot.com/2009/07/thrilling-thulir.html

[![july003](http://www.thulir.org/wp/wp-content/uploads/2009/08/july003.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july003.jpg)

[![july001](http://www.thulir.org/wp/wp-content/uploads/2009/08/july001.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july001.jpg)

[![july004](http://www.thulir.org/wp/wp-content/uploads/2009/08/july004.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/08/july004.jpg)

