---
author: thulir
date: 2009-07-03 18:00:20+00:00
draft: false
title: Newsletter -- Feb 09 to June 09
type: post
url: /wp/2009/07/newsletter-feb-09-to-june-09/
categories:
- Newsletters
---

![Elephant](http://www.thulir.org/wp/wp-content/uploads/2009/07/100_1692.jpg)




Blessed by a few welcome showers of rain in the first week of june, the entire Sittilingi valley heaved a sigh of relief.Temperatures that were soaring at 40-41 degrees Celsius, came down to 34-36 degrees C.The grass, plants and trees responded in a flash to the rains by sprouting fresh, green leaves.The scorched, dry dusty brown summer cloak is gone and the Valley is green and beautiful again.

At Thulir our group of senior students Perumal, Senthil,Vinu, Devagi and Rajammal have grown and bloomed into a very responsible, confident, enthusiastic, sincere group and are now guiding and teaching the younger children.They also take more responsibility in the administration and management of Thulir. For instance, they all worked very hard in May in-spite of the blistering heat. May was supposedly holiday time in Thulir, i.e. we had no classes. But we had more physical work this month than most other times. We had to deepen our well, repair the mud floors and walls of the classrooms,take stock of the books and learning materials. etc... The seniors took charge of most of this work and with very little guidance, completed the work well.


![deepening of well](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_001.jpg)





_Deepening work in our open well_



Three boys from Moolasittilingi village who had wanted to join the Basic Technology course begining in June also came every day in May and worked hard with us. We were really amazed to see this.We had not asked them to work, nor were they getting paid. But here were these 3 teenagers - considered failures in school, spending every day of their holiday working intensely. What was motivating them? The freedom? The feeling of being treated as equals? The space? The fact that there was no compulsion? The example of the seniors working seriously? One doesn't know.


![painting wall](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_013.jpg)





_Painting walls with home made cement-mud paint_




![Electrical wiring work](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_034.jpg)





_Wiring work in Thulir office building_




![Electrical wiring work](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_033.jpg)





**New Batch joins us for Basic Technology Course**

As we write this, new teenagers keep coming in to join Thulir for the Basic Technology Course this year.We had decided that we will start the course from June 15th. Instead of a formal interview or test to select students where some would  have to be rejected, we have asked the interested students to come in for a trial period and participate in all the daily activities. At the end of it, both sides could decide whether they should join the course or not. Three adivasi students from a village near Gudalur in the Nilgiris have come to join the course today [24th June].They have been selected to undergo this course by the Vidyodaya Education Team, who are interested in the concept of this course. We hope they would adjust to this vastly different environment and would complete the course.  Activities have started with the group in the meantime.

One of our dreams this year is to document the classes/ work done in the Basic Technology course and share it through this Blog. So watch out for more posts in this space!

**Summer workshop for the seniors**

From May 25th to 31st we had a workshop on "Learning and facilitating Learning". This was primarily a preparatory session for Perumal, Vinu, Rajammal, Devagi and Ezhumalai; to help them teach the younger students this year.The sessions were a mix of discussions, readings [John Holt, Totto Chan, Sylvia Ashton-Warner etc.], activities,use of teaching aids, trying out creative ways of language and math teaching and practical discussions on problems they encountered last year while teaching the younger children.

**Running**:

Santhosh Padmanabhan continues to inspire and guide the students. With his help and guidance Senthil has taken charge of the running training for the younger kids. A group of 6-8 children practised running every morning at 5. am throughout the summer. Santhosh visited Thulir in May along with Manjula Sridhar of Asha to interact with this group. He took Senthil, Perumal and Ezhumalai to Bangalore to participate in the Sunfeast 10 Km run on 31st May. After the run he arranged for them to visit Ananya School in Bangalore and interact with the students there. More details in the[ Team Asha Blog](http://teamashablr.blogspot.com/2009/06/team-asha-8k-run-2009-train-run-and.html)

On 20th June Perumal Ezhumalai and Vinu participated in the Valley School run.After the run they were able to go with Sudhakar of Mrinmayee to purchase materials required for Thulir, in the Bangalore Market area. These trips outside Sittilingi are valuable learning experiences , offer exposure and increase confidence among our seniors. Thanks to Team Asha and Mrinmayee for making it possible.

**Traning Programme on installation and maintenance of Solar Photovoltaic systems.**

With the help of V.Ramasubramanian of VillageRS Thulir conducted this 3 day training programme in April.We have been using Solar PV systems for lighting and computers successfully for the last 5 years in Thulir. We have acquired some knowledge and skills in installing and maintaining these systems.Over the years we kept getting requests from various friends / groups to train some of their personnel. Hence we decided to hold an organised training programme for 3 days in April. Mr. V.Ramasubramaniam, of "VillageRS", an expert in alternative energy systems and the person who helped us design and install out PV systems , was the Resource person. We had participants from Hunnarshala Bhuj; Mrinmayee, Bangalore; ACCORD Gudalur; Payir, Thenur and Sittilingi. The group was a hetrogenous one with varying academic levels and langauages, and backgrounds. So planning the sessions was challenging. The Sessions were a mix of theory and practicals and we got a positive feed back from most of the participants. For us it was a very good experience. For Senthil and Perumal, who anchored some of the practical sessions, this was a confidence boosting experience.They could see some value in what they had been doing as a matter of routine.


![Solar PV training Programme](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_017.jpg)





_A session on testing of  Solar PV panels_




![Solar PV training Programme](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_015.jpg)





_Perumal Teaching_ _assembly of white LED bulbs_



**Organic Vegetable Farming**

Interest has been sustained and the plants grew well. So we got some organically grown  vegetables for the Thulir kitchen. but the water crises and the deepening work of the well in May affected our farming, and we haven't got the expected yield for the kitchen from these plots. With the new batch of students in, enthusiasm has increased and they are working to get one small portion of the land for rice cultivation. This year so far the rains haven't been good and the well water is still not enough for agriculture, so we are keeping our fingers crossed and waiting for rains.


![harvesting onions](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_030.jpg)





_Harvesting onions_




![organic garden - working on](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_011.jpg)





_Getting the vegetable patch ready_




![organic garden - healthy plants](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_020.jpg)





_An experimental raised bed for vegetables in Thulir, after seeing in Auroville!_




![organic garden - corn harvest](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_028.jpg)





_Our corn harvest!!_



**Evening Students Classes**

With the begining of the new academic year in June, we had a big spurt in the children coming to Thulir for the evening classes. With a few weeks of settling down and classes/ activities/schedules falling in place, now there is some order and consolidation. As we do usually, we have made groups on the basis of academic levels and age and classes have started in right earnest.

**Writing Stories**

One of the activities we did end of last academic year was to get the children to write their own stories and produce books and then read it out loud to the rest of the group. It was great fun and the children did the work very seriously. Already there is an increase in confidence levels when it comes to reading and writing among the younger children; this year we hope to work further and go beyond just the basics.


![Reading out self written Stories](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_024.jpg)





![Reading out self written Stories](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_022.jpg)





![Reading out self written Stories](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_023.jpg)




**Govindammal **

Govindammal, one of our seniors, got married and has moved out of Sittilingi.

We wish her all the best and a happy married life!


![white LED bulb](http://www.thulir.org/wp/wp-content/uploads/2009/07/blogpic0906_002.jpg)





_A lamp assembeled in Thulir using white LEDs, waste CD and papermache work_
