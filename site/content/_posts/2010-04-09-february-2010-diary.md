---
author: thulir
date: 2010-04-09 17:10:04+00:00
draft: false
title: 09 February 2010 Diary
type: post
url: /wp/2010/04/february-2010-diary/
categories:
- Life Skills Course Diary
---

**Bamboo and LED torch making workshop in Rishi Valley school**

Perumal and and vijayakumar went from Thulir along with Anu and Krishna to conduct a 5 day workshop in the Rural Education Centre run by Rishi Valley School. The aim was to introduce hands on work to class 6 students. The workshop focussed on making LED torches using bamboo body as well as one using PVC pipes. This included basic measuring and cutting skills; understanding basic electronics concepts such as circuits, current, resistance and voltage; and soldering skills.

[![Rishi Valley workshop](http://www.thulir.org/wp/wp-content/uploads/2010/04/mar14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/mar14.jpg)

[![Rishi Valley workshop](http://www.thulir.org/wp/wp-content/uploads/2010/04/mar13.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/mar13.jpg)

[![Rishi Valley workshop](http://www.thulir.org/wp/wp-content/uploads/2010/04/mar12.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/mar12.jpg)

**Japanese citizens delegation visits Thulir**

A delegation of Japanese citizens sponsored by their Govt. visited Sittilingi. They also visited Thulir and interacted with the students, looking at the various activties. They were particularly interested in the organic farming activity, as a few of them are organic farmers and are familiar with natural farming methods of Masanobu Fukuoka.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/100_4951.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/100_4951.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/100_4949.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/100_4949.jpg)

**Bee Keeping update:**

When we had the Bee keeping workshop conducted by Mr Justin from Keystone, he demonstrated how to make artificial queen cells and introduce them into the colony, so that a new queen can be produced by the colony. This is a very useful technique to learn as this helps in multiplying colonies. The cells that were introduced during his visit into a colony were ready after 10 days for introduction into new colonies and we were able to shift the cell this month. Her are some pictures showing this.


[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb0102.jpg?w=300)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb0102.jpg)




[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb011.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb011.jpg)


We also learnt to make bee boxes using bamboo; and made a couple more, so that we can try using them.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb009.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb009.jpg)

**Ant proofing Bee boxes:**

The concrete saucer like pans that we have been fabricating the past weeks were finally ready for installation. The water filled "moat" deters ants from climbing over onto the bee boxes and attacking the colonies which usually results in the bees abandoning the boxes.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb006.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb006.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb001.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb001.jpg)

**Paddy cultivation / organic farming:**

The new field is doing well so far. We prepared a simple organic input called "Amirthakaraisal"  [ using cow dung 2.5 kg, , cow urine 2.5 lts, jaggery 0.250 kg  and water 25 lts -- for 25 cents of land], and sent to the field while irrigating.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb004.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb004.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb005.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb005.jpg)

This month the raised bed onion was ready for harvest and we managed to get 2 kgs.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb002.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb002.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb003.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/04/feb003.jpg)
