---
author: thulir
date: 2010-02-07 23:23:53+00:00
draft: false
title: 07 December 09 diary
type: post
url: /wp/2010/02/december-09-diary/
categories:
- Life Skills Course Diary
---

**Measuring areas:**

Having  learnt  basics of measurements and calculating areas and volumes,  it is time now for learning to apply to real life situations [also to understand the concepts better!]. So we started with measuring the Volleyball court [see pic below]. The next stage was to measure the paddy field that we are levelling and getting ready.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec01.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec01.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec02.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec02.jpg)

**Organic paddy:**

The test plot is doing well and the growth fairly impressive. It should be ready for harvest by Jan.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec03.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec03.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec03a.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec03a.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec04.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec04.jpg)

**Gardening -- vegetable patch. **

Meanwhile the vegetables in the organic patch is also doing well! We got brinjals, beans, pumpkin and bananas.

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec05.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec05.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec06.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec06.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec07.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec07.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec08.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec08.jpg)

**Bee keeping  --  making a base for the bee boxes to keep ants out: **

Our Bee boxes get attacked by ants and this usually drives away the colonies. We lost a couple of colonies this year due to the ant attcks. One solution we are working on is to make a base for the box that can hold water [like a moat!] and keep ants away. Here are pictures of this base being made in concrete. We made a mistake while removing the concrete from its mould and it broke!! We need to try it again, maybe this time with some reinforcement inside so that it wont break!

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec09.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec09.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec101.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec101.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec11.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec11.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec12.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec12.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec13.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec13.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec14.jpg)

**LED lamp for cycle dynamo:**

Sanjeev and Anita visited us end December. They did a project of designing and assembling white LED based headlamps for bicycle that can run on dynamo!. This gave the students an opportunity to see how an engineer designs a product! Sanjeev had not done this before and so he lead them through the process of first taking measurements of the dynamo output so that the required circuit could be designed.

They have written a detailed report in their blog site [Research @ Thulir](http://smallisbeautiful.blogspot.com/2010/02/research-thulir.html)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec15.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec15.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec16.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec16.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec17.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec17.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec18.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec18.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec19.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec19.jpg)

**New year Greetings:**

Being the time of the year when one sends out seasons greetings, a number of greeting cards were made in Thulir to send to friends. Here are some samples!


[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec20.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec20.jpg)


[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec21.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec21.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec22.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec22.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec23.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2010/02/dec23.jpg)
