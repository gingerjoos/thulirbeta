---
author: nimda321
date: 2016-11-17 09:07:25+00:00
draft: false
title: Newsletter July-October 2016
type: post
url: /wp/2016/11/newsletter-july-october-2016/
categories:
- Newsletters
---

Many new and exciting developments are taking place at Thulir!





### Creation of the new School




After innumerable designs, discussions with the teachers and students and changes, we have finally started on the foundations of the classrooms last month! We would like to create an environment which is aesthetically pleasing, cost effective, environment and child friendly while satisfying the government norms.




The campus will be built by the local artisans we have trained over the years and it will use renewable energy as far as possible. Kumar is the latest addition to our Thulir team. He is from Naikuthi village and will be helping us with site supervision.


[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_23.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_23.jpg)


### Gift Of Water


The Thulir Alumni under the leadership of Perumal dug an open well in the new school land. It was a pleasure to see them organise and conduct the whole operation very professionally.

We are really happy to have struck water at 12 feet and now we have a well full of water! Thanks to the water divining by Regi.


### [![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_01.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_01.jpg)




### The first cottage


Sakthivel, Jayabal and the present batch of BT students. built a small cottage to store materials and for a caretaker to stay.

A new technique of mud walling - _in situ mud concreting_ - was tried out here. We mixed mud, debris from the well digging, some stones and a small percentage of cement and poured it into bamboo shutters to make the wall.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_03.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_03.jpg)


### Tree planting Mela




Govinda and Leela from Marudam School organised a tree planting weekend in the new land. Teachers and students from Marudam brought energy, enthusiasm and tree saplings and joined with our teachers, students to celebrate nature and friendship! The immense satisfaction of work like this and the bonding with like-minded people overshadowed the hot punishing weather and the hard physical work.


[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_02.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_02.jpg)

The Thulir team continued the planting and watering work through the rest of the week. In spite of the monsoon playing truant in the month since, the saplings are happy so far.


### [![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_09.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_09.jpg)




### Teacher training workshops


At the beginning of this academic year, Poornima conducted a two day workshop for teachers at Thulir. Later, Jessica from Marudam Farm School visited Thulir and conducted a workshop on Pedagogy through movement and games.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_20.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_20.jpg)

Nikita from Marudam initiated an impromptu craft workshop for teachers when she visited Thulir with Ram.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_181.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_181.jpg)

Chitra, Sinthamani and Sasikala from Thulir attended a teacher training workshop at Marudam from October 20th to 22nd

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_081.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_081.jpg)

We are grateful to our friends at Marudam, for providing these additional learning opportunities to us. Training programs such as these have indeed broadened our horizons and will definitely help us to take on the huge challenges of the new school.


### Music and Dance workshop


Shirly and Baby, the founders of Kanavu Gurukula at Wynad visited with their daughter Shanthi from 20th to 23rdOctober and conducted a song and dance workshop. They got all of us to shed our inhibitions and dance.

Music and dance feed our souls and it is a shame that hardly any of us sing or sway to music nowadays. Even adivasi communities are not engaging actively with these art forms but are just becoming passive consumers of it. We hope they will visit regularly and continue these activities.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_24.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_24.jpg)


### Parents Meeting


We continue to engage with parents, as we strongly believe that an educative process cannot happen in isolation and parents play a critical part in their children’s learning. We had two parents meetings: one in June and one in September.

Parents actively participated in these meetings and they were very keen to know about their child’s academic performance. Teachers engaged them with the works of their children and provided constructive feedback.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_28.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_28.jpg)


In an open discussion, most of the parents expressed immense satisfaction with the school and they were happy about their child’s progress.  The recurring question about children climbing trees was discussed again in this meeting, with some of the parents apprehensive about tree-climbing and others confident about the benefits of it.


[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_14.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_14.jpg)


We stressed the need to question ourselves as Adivasi parents, whether our negative response to tree climbing is due to the outside urbanised society's influence; which makes us feel that our way of living or being with nature is "backward/wild" and must change? How do we educate our children to learn the skills of the modern world while not losing the inherent positive qualities of the local community?


[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_27.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_27.jpg)

Overall, the session was filled with buoyant enthusiasm with cheerful exchanges, feedback, questions and concluded with a wonderful performance by the children. We look forward for such meaningful participation and interactions from the parents in the upcoming meetings.


### Participation in the Bangalore Marathon


On October 16th, Sakthivel, Prabhu, Annamalai, Mohan, Perumal, Raghu, Sriram and Karthi participated in the Bengaluru Marathan 5K run

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_251.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_251.jpg)


### Participation in the Craft Week at Marudam


Sakthivel was a resource person for the Marudam craft week and guided students in bamboo craft work. The BT course students participated in the craft week and learnt various crafts.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_04.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_04.jpg)


### Scratch


Sreyarth shared his knowledge about scratch programming with the children – Scratch, is a free programming language, with which you can create your own interactive stories, games, and animations.

Children were immediately hooked and we could witness the immense satisfaction in their eyes. Moreover, this process encouraged them to use English naturally without explicit instruction.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_21.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_21.jpg)

In a way, "scratch" not only helped them to gain programming exposure but also provided a great language learning experience. We intend to use scratch programming as a creative learning tool in the coming months, watch this space…


### School Trip to Mel and A.K. Thanda




Our children come from six villages in the valley - Thanda, Sittilingi, Moola Sittilingi, Velanur, S.Dadampatti and Rettakuttai. We realised that many of the teachers and students had never visited each other’s villages. So we decided to organise exposure trips to different parts of the valley.




The first trip was to Thanda. It was an eye-opener to the fact that money, exotic locations and costly gadgets are not necessary for children to have a good time.


[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_261.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_261.jpg)

Melthanda and A.K Thanda are Lambadi hamlets in the valley. The Porgai artisans who create the beautiful embroidery are from here. We all carried our lunch boxes and children discovered great nooks, corners and slopes under various trees to play and have a great time.


### Project based learning


In our project based approach to learning, every month we choose a theme and base our activities around it. Through this process, children gain knowledge and skills by working over a period of time - learning about the concepts, investigating and asking questions.

This time we chose three difference themes: Colour, Food & Five senses.

Colour: We learnt about different colours, their formation, making natural & artificial colours, colours in our lives etc., children were encouraged to express their interpretation of colours through multiple art forms -  watercolour paintings, play and songs


[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_06.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_06.jpg)


Food: We learnt about food, traditional & modern, methods of cooking, nutritional value of food along with theoretical & practical activities related to food.


[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_17.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_17.jpg)


We also tried our hands in cooking some traditional recipes.


[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_16.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_16.jpg)


Five senses: We learnt about the five senses – Sight, Touch, Smell, Taste and Hearing. Children experienced these sense expressions in their myriad variations. The setting of the school amidst nature provided ample opportunities for us to tune in to these oft-neglected sense experiences.

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_12.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_12.jpg)

Various games and activities, in which each of the senses have to be used in a discerning manner, were held. Learning about the world around us, not only intellectually but with all our senses, was fun.


### Visits/Visitors/Volunteers





	  * Anita Balasubramaniam visited us in September. She conducted a small workshop with a handloom and taught us how to weave. Teachers and some children started to learn weaving

[![](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_13.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2016/11/Newsletter_Jul_Oct_16_13.jpg)



	  * Vidya – a Young B.Ed student from Dindigul, volunteered in the school for 2 months.
	  * Sunder – from Delhi is volunteering here and taking English classes. Sunder doesn’t know Tamil and it is heartening to witness our children interacting with him despite the language barrier.
	  * Accompanied by Senthil from Helikx open school, 10 students and 1 professor from Kumaraguru college of Engineering visited in September.
	  * 30 teachers from Teach For India, visited in October and interacted with our children and teachers
	  * About 50 students from VIT, visited the valley and witnessed the Alternative building technology implemented here.
	  * Krishna was invited by the architecture department of VIT as the jury for their rural design project.
	  * Anu was invited to talk in the Children’s day celebrations and valedictory function of the Helikx Open School, Salem.

As we embark on this journey towards building a new school, we need all your support and encouragement. Please spread the word about the construction of the school and help us fund-raise for the buildings. Every small bit counts and  let us together create a meaningful learning space for the children & adults.


***************************
