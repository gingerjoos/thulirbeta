---
author: thulir
date: 2006-03-04 08:09:00+00:00
draft: false
title: News I March
type: post
url: /wp/2006/03/news-i-march/
categories:
- Newsletters
---

  
Exposure Visit  
  
[![](http://photos1.blogger.com/blogger/8103/1906/320/GF.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/GF.jpg)  
[![](http://photos1.blogger.com/blogger/8103/1906/320/GFjayappa.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/GFjayappa.jpg)  
[![](http://photos1.blogger.com/blogger/8103/1906/320/navadarshanam.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/navadarshanam.jpg)  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
Pics: [clockwise from top left]  
  
1. a simple compost structure at Green Foundation  
2. Jayappa at Green Founation campus  
3. with the Navadarshanam team  
  
  
  
  
  
  
  
We finally decided its time for our senior students and staff to travel out of Sittilingi valley. Lots of village youth who work in Tiruppur had come for the Pongal holidays in January and trigged off a travel itch amongst our senior boys. We were also visited by Shri Jayadeva and Shri Krishna Prasad from Green Foundation, Thali, who presented a slide show of stunning images of seed conservation work done by Thali farmers and also of bio-diverse farms. We decided we must see this work for ourselves and get ideas on how to go about doing similar work here.  
  
Thali, happens to be about 200 kms from here. We also knew of another project named Navadarshanam which has worked on bringing back to life 100 acres of degraded land. So we decided to visit these two places.  
  
First we went to Navadarshanam, where Jyothi and Ananthu along with their team gave us a warm welcome and looked after us really well. Our team felt so at home that by evening, they had joined the residents intheir daily volleyball game and later in the common kitchen pitched in to learn to make rotis! The team speaks mostly in Kannada and Telugu [being a border village], and so it was a good opportunity to learn a bit of kannada too.  
  
Ananthu took us around their land and campus and explained to us the various energy systems that they have installed and are using. [solar pv panels, wind mill, biogas operated genset, and charcoal making oven]. They also have a good food processing unit and we were taken around that too. Our boys were keen to know how the various products were being made and Ananthu patiently gave the recipies and explained how to go about preparing them.  
  
The next day we went to Green Foundation's Centre near Thali. Here we were exposed to the concept of bio diversity and the need for conservation of seeds. We were also shown different techniques to determine seed fertililty/ growing plants with drip/ micro irrigation, different methods of composting and mulching/ preparation of organic pesticides and growth promoters, etc. The high point of the visit was going to Jayappa's farm and seeing the way he has managed to grow a wide variety of plants in a small plot of 20 cents [a fifth of an acre]. We also visited a village seed bank which is being run by the local women's group and talked to them about how they started and achieved so much.  
  
From Thali the next stop was Puvidham school near Dharmapuri. In 2004, students from there had visited Thulir and it ws our turn to make a return visit. Puvidham school has a hostel for its students and our team stayed there. It was a brief stay of 2 nights and a day there and we had a lot of fun talking/ singing and observing a typical day's programme at the school.  
  
  
  
  

