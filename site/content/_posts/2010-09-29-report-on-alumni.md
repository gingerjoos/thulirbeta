---
author: nimda321
date: 2010-09-29 06:59:08+00:00
draft: false
title: Report on Alumni
type: post
url: /wp/2010/09/report-on-alumni/
categories:
- Alumni
---

Report on the full time Teenage Students of Thulir -compiled in April,2010.


### 1. Vediyappan-


was in Thulir from 2004- 2005. While here he rewrote his std.12 exams and passed

worked in Svad-the Sittilingi valley organic farmers association for 2 years. Has

joined a college in Salem to study BA.


### 2. Satya


She came to Thulir to rewrite some of her class 10 subjects . She is now working in her farm.

_Students of the Basic Technology Course in 2006-2007 _


### 3. Senthil.S - higher studies – works in Thulir.


Worked in Thulir for a year 2007 to 2008, helping in administration. He worked for short periods in Timbaktu collective and Payir Trust. He with other students did the photo voltaic system installation in a farmhouse in Tumkur.

Went back to school in 2008. Joined std.11 in The Government school in Kottapatti.

He has just written his std.12 exams. He now works in Thulir.


### 4. Balamurugan – higher studies


Went back to school in 2007. Joined std.10 in the local school. Went on to do his std.11

and 12 in the government school in Kottapatti.

Has just written his class 12 exams.


### 5. Mohan – Higher studies


Came to Thulir with a very bad heart problem and bad health.

While here he wrote his class 10 exams and passed.With help from Thulir

and Asha he had a valve replacement surgery done at CMC,Vellore. Joined a bible college

at Kolar in 2008.He will be finishing this year and plans to work for the church.


### 6. Perumal- Works in Thulir


After finishing his course, he started working in Thulir. He finished his class 10 exams. He learnt how to teach the junior batches, administration, driving, accounts and more electronics. He with other students did the photo voltaic system installation in a farmhouse in Tumkur. He now has a good reputation of having a good intuitive understanding of electrical and electronic equipment. So is often called upon to fix things in the hospital and in other places etc. He helped in installing the solar lighting of the hospital. He went to two projects in Orissa to help Rams install micro hydel power plants. He is now an important part of the teaching team in Thulir.


### 7. Jairam- agriculture


He dropped out the course halfway through the year as he had no one else to mind his farm. He continues farming.


### 8. Madhu- higher studies,own enterprise


He went back to school after the BT course and finished his class 10. He runs the cable TV network and does repair and maintenance for the people in his village S. Dadampatti. He has applied for a job in Boom TV, Chennai.


### 9. Satish- works in a factory


He dropped out halfway through the course due to conflicts with his family. He was pressurised to go to Tirupur and work in the garment factory there.

_The 2007-2008 batch_

this batch did some of the projects of the Basic Technology course but did more academics than the previous batch as most of them were keen to finish their class 10


### 10. Rajammal- higher studies, works in Thulir


She finished her class 10 after joining Thulir. She had an aptitude and interest to teach younger children and so after finishing one year has joined Thulir to learn and teach . She is also doing her second year BA in History from Tamilnadu Open University.


### 11. Devagi- Higher Studies , works in Thulir


She finished her class 10 after joining Thulir. She had an aptitude and interest to teach younger children and so after finishing one year has joined Thulir to learn and teach . She is also doing her second year BA in History from Tamilnadu Open University.


### 12. Krishnan- higher studies


Rejoined school and has finished class 12 from Kotapatti High school. Plans to join Krishnagiri Art's college.


### 13. Sangeetha- higher studies


Went back to school after finishing class 10 exams here.


### 14. Sangeetha .S.- Agriculture


Tried to finish class 10 subjects but failed . Is now capably managing her own large farm.


### 15. Nirmala- higher studies


She rewrote a few of her class12 exams while at Thulir and passed. She has joined a private college for nursing.


### 16. Daivanai- higher studies


She rewrote some of her class 10 exams while at Thulir and passed . She has rejoined school for class 11 and 12.


### 17. Chinraj- Agriculture


Rewrote some of his class 10 exams while at Thulir but failed . He does agriculture now.


### 18. Chitra- Higher studies


Rewrote some of her class 10 exams while at Thulir and passed. Has now finished her class 12 from Kottapatti Govt. High School and plans to join a college in Salem for graduation.


### 19. Arul- drives a tractor


Rewrote some of his class 10 exams while at Thulir but failed. He now drives a tractor locally.


### 20. Ilavarasi- higher studies


She rewrote some of her class 10 exams while at Thulir and passed .

Has gone on to study class 11 and 12 outside.


### 21. Priya- works in a factory


Went on to work in a factory near Coimbatore.


### 22. Vignesh- agriculture


Works in his uncle's fields.

_2008-2009 _

This year additional inputs in teaching , managing and Admin were given to Perumal, devagi, Rajammal and Vinu. So we did not take in a complete new batch.


### 23. Vinu- own enterprise


Came from Kanavu school , Wynad, Kerala. He was at Thulir for 2 1/2 years. He learnt here and also taught the younger children and the subsequent batches of teenagers. He managed the Art and Craft unit At Thulir. He has now gone back to kanavu and started his own Art and craft enterprise there to help the Kanavu community.


### 24. Ezhumalai- Working and studying in Thulir


Joined as an assistant to Perumal . Is now part of the 2009-2010 batch of BT. Students.


### 25. Govindammal- Married , agriculture work


Got married on her own terms – managed to convince her would be husband in an arranged marriage not to take any dowry .

_2009-2010 batch _


### 26. Venkatachalapathy


dropped out of the course as his father passed away suddenly and he was under pressure to work in Tiruppur and earn money.


### 27. Vijayakumar- finished one year of the course . He was also under pressure to go to Tiruppur and earn money.


There are 9 other students studying in Thulir from this batch; including three adivasi students who came from Gudalur Nilgiris to stay and study in Thulir.


### To summarise,





	  * Higher studies-- 12
	  * Working in Thulir- 5
	  * Married-1
	  * Agriculture- 5
	  * Own enterprise-2
	  * working in Factory-4
	  * driving-1

[Some of these heads overlap]
