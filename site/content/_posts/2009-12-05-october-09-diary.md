---
author: thulir
date: 2009-12-05 08:04:25+00:00
draft: false
title: 05 October 09 diary
type: post
url: /wp/2009/12/october-09-diary/
categories:
- Life Skills Course Diary
---

**Building Construction Part 9 – completing the cycle shed work!**

The much awaited cycle shed, finally got ready this month! A few minor works like floor levelling and finishing the walls by pointing were undertaken and the shed is now operational.

This also means we have more workshop space now as cycles were being parked there for want of space especailly when it rained.

[![construction](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct016.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct016.jpg)

[![construction](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct003.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct003.jpg)

**Bamboo craft: making a door using bamboo**!

We needed an extra door to the Thulir Kitchen area. So far, most doors we use are made of ferrocement. We decided we will make this one out of bamboo [much nicer looking!] and thought this would be a good opportunity to learn bamboo craft skills.

Our students are already familiar with bamboo. So all they needed was some guidence in the design of the door. We already had one design so they had to adapt it to the specific opening size here.

[![bamboo craft](http://thulir.files.wordpress.com/2009/11/oct004.jpg)
](http://thulir.files.wordpress.com/2009/11/oct004.jpg)

[![bamboo craft](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct013.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct013.jpg)

[![bamboo craft](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct014.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct014.jpg)

**Sessions on HIV/ AIDS**

We had a visit by Mrs Julie from Nalam Child Development Centre, Namakkal to Thulir, this month. She has experience in working with children / youth in communication skills and also on HIV/ AIDS. She held a couple of sessions for our senior students.

[![HIV / AIDS](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct009.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct009.jpg)

[![HIV / AIDS](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct010.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct010.jpg)

**Continuing electronics classes**

The sessions that Sanjeev had held on electronics was continued this month, so that concepts could be better understood and to maintain continuity. Perumal took these classes.

[![electronics](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct007.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct007.jpg)

[![electronics](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct008.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct008.jpg)

**Vinu helps with Porgai craft production**

The Porgai womens group that does exquisite Lambadi embroidery was making products against a deadline for an exhibition in Bangalore. Unfortunately a flu epidemic in our area affected this group too and they were short of hands to finish their orders. Vinu got an opportunity to step in and draw patterns in fabric that could be sent for embroidery work. This was a good learning experience as Vinu plans to start a craft production unit of his own.

[![craft](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct012.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct012.jpg)

**Theory sessions**

Classroom theory sessions continued with some intensity this month. Math skills are a particularly weak area and we have almost daily sessions on strengthening this area.

[![Theory](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct006.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/oct006.jpg)
