---
author: thulir
date: 2010-09-29 12:29:04+00:00
draft: false
title: Evaluation by Students 2010
type: post
url: /wp/2010/09/evaluation/
categories:
- Evaluation
---

We recently had a round of evaluations of Thulir  both by the smaller children and the young adults.During the first round of evaluation the following people were present -  Teenagers group of 10 students [the present senior batch], Perumal, Rajammal, Devagi, Senthil, Ravi,  Anu and Krishna.The object was mainly to  get a feedback of students' opinions, confidence, skills gained etc.

This was  a session of group discussions interspersed   with a few questions which everybody answered anonymously in writing. The answers were read out .

The background of this was that in May many students from sittilingi got their std.10 and std.12 results. This year saw a significant number  from Sittilingi finishing their class 12 . Many  of them came to Thulir for help in applying for higher studies. The prevailing mood in the village is that everybody should apply for engineering or BSc. (Math). All these students look down at the Basic technology course students as failures. So we thought the morale in Thulir was low.

We started the session by going over our objectives in running the course viz.


- to give them  self-confidence




- give them skills to lead an independent, and satisfied life


The students then listed  thulir's contributions towards achieving this,viz;


money




materials




resource people




education


Then a series of questions were put to them --

“_What do the students give Thulir in return ?” _

Their response was that they give back the following:


Work – both  for the functioning of Thulir and project work




Cooperation – with Thulir and  with each other




Self- iscipline




Learn and teach others thereby helping with Thulir's objectives


“_If we give each one of you money  and ask you to earn a living on your own in the village , would you be able to ? Are you confident to do so ?”_

Answers from all present except 3- Yes [83percent]-

This was a great surprise to us. We had expected them to say we need more education, we need support, skills, etc. In the discussion that followed many of them expressed the view that if the same question had been asked them a year ago, their answer would have been in the negative.This one year of the course had helped them say yes with confidence.

“_Would you like to work  alone on your own or would you like to work as a group with some of your batch mates here? Which do you think will work better?”_

All of them answered that  they would like to work as a group and they felt that would definitely work better.

They were then split into groups and asked to enumerate  what they would do to earn a living and lead  an independent life.

Answers-

Group 1


Farming




Construction contracts


Group 2


Organic farming




Electrical wiring




Furniture- need to learn more




Teaching – need to learn more




Craft work – need help with marketing


Group 3


Electronics




Electrical wiring


Group 4





Electrical wiring




Organic farming




Construction


This listing gave the whole group a lot of confidence and this was plainly visible.

“_What skills do we  require to succeed in each of these jobs?”_

Wiring was taken as an example; and the following was listed as something they have to learn;


Planning




Estimating




Names of  components




Quality/ rating of components etc.




Rates




Quantity of labour




Ability to write about the work done/ Record progress




Costing




Ability to talk to strangers


“_What does a family in the village spend money on normally?”_

36 items were listed .

There was then a discussion as to whether one could survive in the village doing just one job or whether a combination of jobs was required? The groups had already answered this question when each had listed a combination of jobs to survive.


_**Test for Thulir **_


[Evaluation by the teenagers- Thirteen students were given a questionnaire which they filled anonymously.]_ _

_Question 1.- What did you learn in 	Thulir?_

They listed the following:



	  * Basic Maths
	  * To talk to new people
	  * To read  some English
	  * To read  and write Tamil
	  * organic farming
	  * to make torches
	  * bee keeping
	  * To make soap
	  * Wiring
	  * to build
	  * to draw plans
	  * To estimate
	  * Table lamps
	  * ceiling lamp
	  * lamp shades
	  * welding
	  * Calculations and costing in 	organic farming
	  * plumbing
	  * some computer work
	  * to repair cycles
	  * Cooperation and working together
	  * Division and multiplication
	  * To measure with a tape
	  * To measure with a multi meter
	  * To repair motors
	  * Calculation of area of land

_Question 2.-  You had some expectations of learning before you joined Thulir. Did you learn more than that , less than that or as much as you expected?_


More than I expected- 12




As much as I expected- 1


_Question 3.- Do you understand the way things are taught in Thulir?_


Yes	 11




No	 0




Okay	 2


_Question 4.- What  you learn here is_


Useful- 12




Not useful-0




Moderately useful-1


_Question 5.- When compared to school the learning in Thulir is _


more		13




less		0




the same	0


_Question 6. -  To improve teaching / learning in Thulir we need _


More practical classes	 11




More theory classes		  2





_Question 7.- Are these classes 	necessary?_
[the following list was provided and each had to list the things they felt is necessary and list those they think is not necessary]

Activity - necessary





Sports          	            6




Vocations                	10




Art                         	 8




English                        6




Maths                          7




Music                          6




Dance                         2




Farming                      5




General knowledge    5




Discussions                5




Craft                           7




None of them listed any of the activity as not necessary._ _


_Question 8.- I would like to be 	taught the following things further... list _


The following is the summary of the activities that came out of the responses.




a. Electrical Wiring




b. Welding




c. Operating different machines




d.Tailoring




e. Computer




f. Maths




g. Tamil




h. General knowledge_ _


_Question 9.- In thulir, we need to 	be more_


strict-  6




not strict-4




don't know- 3_ _


_Question 10  - _Should we 	continue conducting this course?


a. Yes 	   13




b. No    o


_Question 11.- In  your opinion Thulir has_


failed in its task  		 0




succeeded in its task		 13_ _


_Question 12 .- I award the 	following marks to Thulir_


Average score from 1_3 students   --_ 76%




----------------------------------------





## Test for Thulir




_(with the children of the evening batch)_





_What have you learnt in Thulir?_

It was heartening to note that most children listed reading books and mathematics as the first activity. This has never been the topmost activity in earlier years. Learning through handling materials, activity based learning was always viewed as play and the children would list ‘playing’ as the first activity.

The other activities listed are playing, stories, songs, art, craft, talking to new people, learning about new places, etc.

_What did you like in Thulir?_

Most children have said that they like Thulir, the place itself and the  people in Thulir. Many have said that they like reading books,  listening to stories and doing mathematics here.

_What don’t you like?_

One or two have said that they don’t like the older boys take over the  playground and play . A couple of children have said that there is  nothing about Thulir they don’t like.
But, most children have misinterpreted the question as ‘what they don’t  like generally’ and have said that they don’t like to fight or use bad  language.
_
_
_ Is the evening snack provided in Thulir necessary or not?_

Everyone has said that they like the evening snack.


_ Will you come to Thulir even if the snack is not given?_




_[These questions were for us to find out if the children were coming to Thulir just for the snack or were coming for something more…]_


Everyone has said that they would come even if the snack is not given.

_Would you like to have sessions on all days?_


_[We presently have sessions from Tue through Sat.]_


Everyone has said that they would like to have sessions everyday, except Sundays. [In earlier years we have had demands to have sessions on all days. With free TV in every house they want to watch TV on Sundays. They would like a session on Mondays.]_ _
_Should we teach only from the school textbooks and dispense with other general activities we do?_

The unanimous answer is “No, please do both – but don’t teach like at school”.
_Should we hit children to correct them as in school?_

_[We had a doubt that some parents and children might be expecting the above two and perhaps some kids were forced to drop out as this was not happening.]_

_ _Everybody has said “please don’t hit and continue to teach us kindly and patiently”.
_ _

_Should we be stricter at Thulir?_
_ _

“No” from most children – 4 have said “a little more strict”. A few children have written that when corrected patiently they respond and adhere to rules better.

_Do you understand what is being taught in Thulir?_

Everyone has said that  they understand well. A few have said that they understand better than in school.
_ _

_Some students have dropped out – do you know the reason why they have done so?_


Reasons cited for the children dropping out or being absent now and then
a.Sickness
b.Fear of ghost believed to be residing in the house on the way to Thulir
c.Agricultural work
d.Parents stopping them at home for work or for looking after younger children


_Should we make attendance compulsory?_

No

_Should written work be given everyday?_

Half the kids wanted written work, while the other didn’t want it and wanted a mix of activities.
_ _

_In your opinion, is Thulir succeeding in what it has set out to do?_

Everyone has certified that Thulir has succeeded.
_ _

_Marks awarded by you…._

Most children have awarded 100% to Thulir, some have given one thousand, a lakh [hundred thousand], etc.
