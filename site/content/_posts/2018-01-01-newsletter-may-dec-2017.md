---
author: nimda321
date: 2018-01-01 04:38:28+00:00
draft: false
title: Newsletter May - Dec 2017
type: post
url: /wp/2018/01/newsletter-may-dec-2017/
categories:
- Newsletters
---

We are happy to share the latest news and updates from Thulir.


The past 8 months have passed quite quickly, without our realizing it. From a near-drought situation in May/July to a bountiful spell of rains in August/ September, our journey has been quite exciting.





### **Summer Dance Camp and Street Performance**




We conducted a dance workshop from May 8th to May 15th for the government and private school children as well as the nursing students from the hospital, all of whom didn't get a chance to participate in the last workshop at school. Mr. Mohan from Thiruvannamalai was the resource person for this activity.


On the last night, the students put up a very impressive street performance in the middle of Sittilingi village with hardly any props. Almost the whole village turned up! The students sang, danced and performed a street play. The electricity went off in the middle, but no one minded it or whistled or moved away! The show just went on. People used flash lights from their phones and houses and were silent enough to hear everything. All the villagers helped by diverting vehicles, organizing etc. The fact that everybody took ownership over the program felt very good.

[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170514_190556.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170514_190556.jpg)


### **Water**




****We had a prolonged and scorching summer, which led to a severe water scarcity. The hospital team initiated meetings in the villages to talk to people about water conservation measures. The Thulir team also took part in the de-silting of the check dam in Moola Sittilingi village. It was amazing to see how much could be accomplished without any money or machinery, just by getting everyone together.





### **New admissions and transfers**




We had decided that we would limit admissions this academic year as the new buildings were not ready and space in our present campus was limited. So since March we have had to turn away a number of parents who came to us for admissions.




But the situation in the village changed in July. A new road to Karumandurai town (in the Kalrayan hills) became operational this year and a private school van from there started plying to Sittilingi and canvassing for students. Meanwhile, the local government school started an English medium section. Three nurses withdrew their children from our school to admit them in the private school.




The advantages of having a school van to pick up and drop the students were cited as reasons for withdrawing them from Thulir, as we don't currently have a school van. Influenced by this another 6 children were withdrawn from Thulir.




This incident lowered the morale of the entire team. It also brought home the fact that education has become a commodity even in a remote place like Sittilingi and parents with some salaried jobs would like to purchase what they perceive is the best that money can buy. Their behaviour with relation to this is exactly the attitude of a shopper who drops everything and rushes to the latest brand in town. Some of these kids have been shifted to a new school every year, regardless of the disruption it causes to their education and their emotional development. And very often the paraphernalia ( of uniform, shoes, tie, van, strict discipline and English) is thought to be ' good education’.




The parents in rural areas, maybe because they feel they have a lot to catch up on, are worse than their urban counterparts in expecting their children to perform well and put enormous pressure on even 5 year olds to stop play and study. The freedom our children enjoy and the practical way through which they learn have not been understood by these parents. In the past, some of them have openly expressed their displeasure that our children climb trees or go on forest walks or that most of our teachers are tribal.




But this also opened up many questions for us to reflect and critically examine our work for any flaws in our approach. As teachers we are increasingly aware how our children are flowering in an atmosphere of freedom through the questions they ask, the quality of their work, self-directed learning, taking ownership, their concern towards the environment etc. Perhaps the parents didn’t get enough opportunity to witness and understand the learning process that is happening in the school. So we explored the possibilities of opening up various avenues for the parents to appreciate what is happening at our school, opportunities to reassure the parents that their children are actually learning much more than academic skills. We also made changes to our timetables and now have a dedicated reading time after lunch. This small change brought in a complete transformation in our childrens’ reading abilities. It is wonderful to watch our children quietly engaging with a text they chose. It has helped them with reading and spelling as well as boosting their confidence in picking up any story book.




We realised that most of our parents in the valley expect Thulir to help their children to communicate in English and many of them wanted home assignments for their children. We wanted to take an approach that allows us to meet the expectations of the parents without trampling on the freedom of our children. So, we have started giving creative home assignments to our children. However, it is optional. Some of our children want more assignments while others don't do homework. We also started allowing them to borrow books in order to read at home. We made a number of our own books. The picture books made by our teachers were an immediate hit.




Just after this, some parents from Erattaikuttai, S.Thadampatti and Thanda approached us saying that they wanted to withdraw their children off other private schools and admit them in our school as education here was much better and children were happier. Five children joined us. We now have 41 children coming from 5 villages to our school.





### From Scarcity to Sufficiency




As we noted in our last update, the open well – our only source of water – was almost dry and a severe water crisis seemed imminent. Our well at the school site was completely dry. During our discussions, we even considered closing the school early and stopping construction for a while.




Eventually, it was decided to dig a 300 ft. bore well to supply water to the school and for other domestic uses. It was unanimously decided to use a hand pump instead of an electric pump, since that would help us limit our consumption.
[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171227_100427.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171227_100427.jpg)




The sinking of the bore well happened with great fanfare, excited children and teachers watching the spectacle with wide eyes. At our new school site, there was no water. We deepened our well to another 10 feet, but it was completely dry by July.




Everything changed in August & September, with frequent showers totaling 9 cm (Aug) and 16 cm (Sep). This allowed our well to recharge and at present we are hopeful that we should be able manage until the next monsoon. The stream next to our campus, which was dry for the past 3 years, flooded with water and it was an exciting time for everyone in the valley to see the river flowing after a long interval.


[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171229_161041.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171229_161041.jpg)


Due to the rains, our morning walks with our kindergarten group has been all the more exciting with lots of bugs, slugs, frogs, birds, varieties of creepers, climbers, flowers to watch and observe. The teachers are having a tough time trying to answer the barrage of questions from the children.





### **Tree Planting drive**




During September children from Marudam Farm School visited Thulir and camped at our new school site. Govinda and team brought tree saplings from Thiruvannamalai. Teachers and children from Thulir and Marudam collaborated and planted these trees on the new school land.




Last year, we had planted around 100 indigenous tree saplings. Of these, 90 have survived despite the tremendously hot summer and water shortage. This year we also made rainwater collection ponds and bunds. This has helped to recharge our well.


A big thank you to The Forest Way and Marudam farm school for their continuous support with our efforts.

[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170909_113828.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170909_113828.jpg)


### **Project based learning**




****As part of our ongoing project based learning approach, we decided to revisit the  project on plants. Our focus has been to help our children observe the wonders of the plant kingdom around us and understand the concepts practically. Every week, children started the project activity by singing traditional songs related to plants.




Since this project was all about plants, we focused on the life cycle of a plant from seed, root, stem, leaves, flowers and covered many topics about plants like seed coat, mono/dicotyledons, root systems, leaf arrangement, venation and conducted experiments to differentiate the soil constituents, to test water retention capacity of the soil, to observe the seed germination process and the root nodules.




Our children were so excited about these activities, they started observing and labeling all the small saplings they saw in their surroundings. The excitement didn’t subside as every day we saw one child or an other curiously observing a strange looking plant and trying to understand if it is monocot or dicot or pulling out small plants and grasses to observe the root system. Of course, they were all re-planted by the children themselves with equal enthusiasm.


[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170830_095819_HDR.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170830_095819_HDR.jpg)


It was a wonderful experience to see the children collecting different types of flowers, leaves, busily making prints, dissecting flowers, pressing them, asking many questions in the process. We had an interesting time with lots of fun and the allocated time wasn’t sufficient. Currently, we are doing a project on Food & Nutrition.





### **Parents meeting**




****In June we convened a parents meeting. We held an open dialogue on what it means to have a school like Thulir, its necessity, who supports it, how its expenses could be managed etc.




This was followed by a discussion about the parents’ contribution towards the school expenses. After an intense discussion, all the parents came to a consensus towards the quantum of their contribution towards the nutritional costs of their children.


[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170913_094033.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170913_094033.jpg)


In September, we held another meeting with the parents. We wanted the parents to understand that their children are not aimlessly frolicking in Thulir but are engaged in an integrated learning environment. So this meeting started off with a performance by the children. Then each child went up to the stage and showed something they had learned at school. Some read an excerpt they chose from a book, others told the audience a story or sang a song. The older group demonstrated solar/lunar eclipses through simple experiments and performed a wonderful puppet show.


[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170913_095151.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170913_095151.jpg)


It was evident from their faces that the parents felt reassured that their children are not just learning academics but are growing up to be a well rounded person. In a discussion that followed afterwards, some of the parents expressed their satisfaction with their children’s progress. They appreciated the individual care given to each child in our school. The recurring theme of “why Thulir? what are we trying to do?” was discussed again, with parents actively expressing their concerns and questions.


[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170913_102357.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20170913_102357.jpg)


It was also decided in the meeting that the first Wednesday of every month would be an open discussion day where parents and anyone else who was interested in Thulir could visit and take part.





### **Yoga classes**




****Yoga training by the Yoga Vahini team for the nurses and our teachers is happening regularly in the hospital and we hope our teachers will be able to apply the knowledge they gain in their classrooms as well.
Parthasarathy, Mrinalini and Priya of Yoga Vahini did a yoga session for the children in Thulir in October. This was a not only delightful for the children but a teacher-training session for the teachers as well.





### **Participation in the Craft Week at Marudam Farm School**




****Two teachers and 13 children attended the craft week at Marudam in November and learnt various crafts: Making beautiful artifacts from coconut leaf, palmyra leaf, banana fibres, coconut shells, stones, beads, waste paper etc. They came back highly enthused! Sakthivel went as a resource person to teach bamboo craft.


[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171121_111105.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171121_111105.jpg)


### **School Visits**




****Children from Little Grove Hyderabad, Shibumi Bangalore, CFL Bangalore, Payir Thenur, and Marudam Thiruvannamalai visited us this academic year.





### **Government Approval**




****We have started the initial steps for obtaining government approval for the new school. We met the D.E.O in Dharmapuri and the A.E.O in Harur in this regard. The A.E.O visited our school. As advised by her, we also applied for the UDISE number for the school. We were invited along with other schools to a meeting in Dharmapuri. We will be able to move ahead in the approval process once we complete 6 classrooms, an office room and toilets and get the building stability certificate.





### **Palmyra Leaf Workshop**


****Thangammal & Esakkiammal from Kanyakumari conducted a workshop in December on making handicrafts using Palmyra leaves.

[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171214_120240.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171214_120240.jpg)


### **Organic farming**




****We harvested Samai in October and planted Urad dhal, Sesame, Horse gram. Vegetable gardens created by our kids and Sakthivel are fully functional supplying Tomatoes, Ladies finger, Chillies, ridge & bottle gourds, to our kitchen.


[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171128_095158.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171128_095158.jpg)


### **Visit to Dadampatti**




****We visited the hamlet S.Dadampatti during the last week of December. The purpose of this visit was to try and understand, as a team, the milieu of the child, its family, social life and challenges faced. The teacher team interacted with the parents and elders in the village.


[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/2017-12-30_11-14-26.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/2017-12-30_11-14-26.jpg)


### **Visitors & Volunteers**


****We had many visitors and volunteers during this year. Here is a brief summary of their visits.



	  * Niru from Australia volunteered in Thulir during September. Niru talked to the team about counselling techniques and various craft activities.
	  * A Doctor couple, Dr. Balasubramanian and Dr. Aruna visited us early this academic year. They expressed their interest in supporting our efforts. When they went back they talked to their classmates about Thulir and brought a group of 15 doctors (all from the 1985 batch of Stanley Medical College) to visit us on October 22nd. We are grateful to them for their encouragement and support.
	  * Poornima from Marudam Farm School conducted a workshop for the teachers. The session was very lively and interactive, filled with games and activities. Our teachers thoroughly enjoyed it and we were once again reminded of the fact that learning can indeed be fun.
	  * The hospital receives medical electives from various countries. Several of them visited Thulir as well. Joan C. Dew from Taiwan and Victoria O’Dore from Honkong talked to the children about their respective countries.
	  * Dr. Sridhar Santhanam from CMC Vellore visited Thulir and interacted with the team. Our team had a lively session with Dr. Sridhar about children, their development, health and the adverse effects of cellphones/computers on children.
	  * Mr. Thomas from Chennai, did a Ventriloquism show at Thulir and everyone enjoyed the performance.
	  * During their visits, Rupa from Shibumi, Nagini from CFL interacted with our teachers about their respective schools, their approach towards education, challenges etc.
	  * Mr. Singaravel & Mr. Kim from Samsung visited Thulir on the 9th of December
	  * Akshatha from Karnataka visited Thulir in November and Bipin from Coorg visited us in December for exposure and guidance. You can read about Akshata’s experience [here](https://drive.google.com/file/d/1xp4ZpkN8V9KQ5RgNRti01yx0xvTXoL9-/view?usp=drivesdk)
	  * Dinesh, an architect from Adhiyamaan Engineering College is interning at Thulir.
	  * Gautam from Hyderabad and Florencia from Argentina visited us in Dec and volunteered at the construction site.
	  * Maya Muhlemann from Switzerland is currently volunteering at Thulir.






**School construction**


****The walls of six classrooms have been built. Of this, the roofs of five classrooms have been laid. The walls of the sixth classroom are just being finished. Roofing elements are yet to be fabricated. Flooring, whitewashing, doors and windows of all the classrooms are also unfinished. We hope to complete the office, staff resource room and toilets before the start of the next academic year.


[![](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171229_161053.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2017/12/IMG_20171229_161053.jpg)




 We request all our friends and well wishers to spread the word and help us fundraise for the buildings. A pamphlet that can be shared online can be found in the support us [page](http://www.thulir.org/wp/support-us/).




  We wish you all a very happy & rewarding 2018!




******



