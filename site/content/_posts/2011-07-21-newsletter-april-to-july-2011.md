---
author: nimda321
date: 2011-07-21 18:07:20+00:00
draft: false
title: Newsletter -- April to July 2011
type: post
url: /wp/2011/07/newsletter-april-to-july-2011/
categories:
- Newsletters
---

[![](http://www.thulir.org/wp/wp-content/uploads/2011/07/photo0008.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/photo0008.jpg)


## Class 10 exams, and an interesting Workshop


April is the month for public exams and 3 girls and 5 boys wrote the class 10 exams.

In April, Jeyaprakash, an Architect friend took a two day workshop along with Yedendra Srinivasan , on using the 3D modelling software "Sketch up" and on Data visualising techniques. Jeyaprakash after doing an introductory presentation, took us through a demo of how to use the Software. This was followed by a day long session where students got an opportunity to try the software -- initially following step by step instruction and then branching off to explore various possibilities with the models they were working on.


## [![](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc00914.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc00914.jpg)


Yedendra showed how we can link the disparate data we have gathered for instance, weather data and attendance in Thulir, and visualise it in a manner that is easy to see patterns/ relationships.


## Learning to Repair Motors and more Electronics


In May, there was a workshop on motor winding and repairing for the senior batch. Visiting Resource persons from Chennai, Mr Anand , Mr Karthikeyan and Prof. Ravindran taught the students. Ramasubramanian and Sanjeev took theory classes on the basics of Electricity and motors for both the batches.


## [![](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc00943.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc00943.jpg)


Sanjeev did an intensive course for the girls and the junior batch on electronics, computers and building several exciting circuits. He also taught them  to do several projects using the programme called Scratch.

We took a summer break from May 15th to June 1st.


## Start of a new academic year and our Seniors graduate and move on




## [![](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc01216.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc01216.jpg)


In June, it was our annual ritual of repairing / repainting  Thulir buildings. The mud walls and floors were re-plastered and repainted  wherever required. The field had to be ploughed to get it ready for the next sowing season. The Permaculture beds needed some attention. It was also time for us to plan activities for the coming year.

In June the results of the class 10 exams came out. All the seniors who appeared had passed.It was time for them to move on to the next stage of learning . So we started exploring options for each individual student according to their preferences and aptitude.

Sakhtivel had already gone to Bangalore to learn wood working skills from Vanya and Goutham in January.He liked the experience and wanted to continue.

Srikanth finished his training in wood craft from Vanya and Goutham. He went back to Gudalur and has started making learning aids in wood at Vidyodaya school.

Kumar felt it was time for him to take on a job in Sittilingi. He was clear that he did not want to go for any further education/ training outside. He has joined the TTI [Tribal Technology Initiative] workshop in Sittilingi on a full time job. He has also learnt Tailoring and has been stitching clothes for the villagers. There is a need for a specialist Tailor for the crafts initiative in Sittilingi(Porgai) and they are now trying to arrange further training for him.

Theerthammal decided to join Thulir as a staff. In the past months she has been quietly taking on minor responsibilities. Devagi and Rajammal are training her in the management of the Thulir Kitchen.

Chandramathi wanted to go outside and pursue further studies, but her family was reluctant to send her away alone to an unknown place. We talked to them and helped her to apply for a course in Medical Lab Technician at Gudalur, a place and institution we are familiar with, and which we thought the parents might find agreeable. But after securing admission, on the last hour her parents changed their mind. She is now applying for a nursing course at the Sittilingi hospital.

Jayabal, has also joined Sakhtivel in Bangalore to undergo a similar training in wood work under Vanya and Goutham. Since he is inclined towards art and craft work, we hope he will benefit immensely from this exposure. He is very young and might find being away from home a bit of a challenge, though.

Perumal, Ezhumalai and Chinnadurai wanted to look at further education possibilities. We contacted several places to search for vocational certification, as all three of them are very good at vocational skills. We located Govt. ITIs nearby where they could apply. Perumal has a flair for electronics.So we  thought Perumal may benefit from a spell at a company where he could learn more electronics related skills; Sanjeev contacted a company in Bangalore where he could apply for an apprenticeship.

The education scenario at Sittilingi valley is changing rapidly these last few years. In 2004, students finishing class 10 was a rarity. In the last three years, more  and more students have finished class 12. The number of students going outside to join college has increased. The trend in the past few years is to do BSc Math with the idea of doing a B Ed then and applying for a Govt Teacher's job in future. People look down upon vocational skills. This line of reasoning says any vocational based training like ITIs or even a Diploma in Engineering is not good.

So, after looking at the possibilities and after talking to many people, all three of them have joined the Kottapatti Govt. School to study Science. This being nearby they can stay at home and commute; and so help in the family Farm. This also has local approval, as the Village thinks BSc [preferably BSc Math] with BEd is the way to go!


## A day long hike


[![](http://www.thulir.org/wp/wp-content/uploads/2011/07/img0026a.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/img0026a.jpg)
Endowed with a beautiful location, we try to walk into the hills to enjoy the forests that surround us.It is an occasion to show the students that we respect their knowledge of plants and forests and to show them how much we appreciate the beauty of their homeland.They too love being outside but very often they take all this for granted. We hope to see a feeling of pride and ownership of the natural resources and the need to conserve them, growing among our students.

On Saturday the 2nd, the younger children took us to a viewpoint rock on a hill nearby.We were quite out of breadth trying to keep up with them!! In some places on the rock face they would whiz past the thorny bushes whereas we grown ups had to crawl on all fours under the bushes! On Sunday the 3rd, the older students and staff took off on a day long trek to a waterfall high up on a ridge on the Sitheri hills. From there the view of our valley was incredible!
[![](http://www.thulir.org/wp/wp-content/uploads/2011/07/photo0002.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/photo0002.jpg)


## Sports Day


After the first sports day held last year which turned out to be an event everybody enjoyed, we thought we should hold one this year. So on July 9th and 10th we held Sport  events. It was very similar to last year -- having more or less the same events. This year we lacked enough volunteer power to clear up our neighbour's land and make tracks etc., so decided to hold the events in the Thulir play ground itself. There was a festive atmosphere and a lot of fun. Many children, even those who don't come regularly to Thulir, participated. This year too we gifted a book to all the 60 participants.


## [![](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc01247.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc01247.jpg)




## [![](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc01319.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc01319.jpg)




## [![](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc01498.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc01498.jpg)




## [![](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc01472.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/dsc01472.jpg)




## Interactions with the Government School


We have been always interacting with the students of the Government schools as almost all the children who come to Thulir in the evenings go to the Government school. But the children who come from far away villages and have to take a bus back have been unable to come here. We have conducted some science experiments for them earlier but were unable to do anything for them regularly due to various reasons. The present Head Master has been aware of our work and has requested us to take some science, computer and General awareness classes there. So we have started going there once or twice a week. There has been a good response so far. He has also been sending students to Thulir to learn Basketball from Siddharth, who is home on holidays.


## A Cycle trip


On 18th July, the older boys and Siddharth went cycling to Theerthamalai- about 70 km totally. They enjoyed themselves thoroughly. The sky was overcast and the Weather pleasant, making it ideal for the trip.

[![](http://www.thulir.org/wp/wp-content/uploads/2011/07/bike-to-theerthamalai-6.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/bike-to-theerthamalai-6.jpg)

[![](http://www.thulir.org/wp/wp-content/uploads/2011/07/bike-to-theerthamalai-3.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2011/07/bike-to-theerthamalai-3.jpg)


_As always, we look forward to your comments and feedback!!_




************
