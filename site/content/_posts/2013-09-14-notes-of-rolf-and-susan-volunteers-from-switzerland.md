---
author: nimda321
date: 2013-09-14 07:56:08+00:00
draft: false
title: Notes of Rolf and Susan, Volunteers from Switzerland.
type: post
url: /wp/2013/09/notes-of-rolf-and-susan-volunteers-from-switzerland/
categories:
- Reflections
- Reports
---

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/r1.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/r1.jpg)


### Coming to Thulir


We, Susan and Rolf, a couple from Switzerland, 43 and 53 years old, lived and volunteered for 6 weeks in Thulir from the 15th of November to the 29th of December 2012. I (Rolf) heard the first time of this place from a friend of Anu and Krishna three years ago. When we got the opportunity to make a break in our jobs as teachers in Switzerland, we read the Thulir website and asked Anu and Krishna about the possibility of volunteering in Thulir. After two holidays of three weeks in India, both of us wanted to return for a longer time and not only as tourists, but with the possibility to stay and volunteer in a place. For us it was also important, that this place should be run by Indians and not by western or religious development aid organisations. After the okay of Anu and Krishna, we got more information about Thulir and what we could do there by mailing with them.
But when we finally arrived in Sittilingi, we realized quite soon, that we did not know that much and that we would have to find out and get used to many things. A great help for us was the warm reception by Krishna, Anu and all the people living in Thulir. Also the information we got from Florence, a French volunteer, who stayed already there for some weeks, made it easier to start our stay in Thulir.


### Living in Thulir


We lived in an apartment with a living room, a bedroom and a small bathroom with toilet and shower. After some days we had fitted out our rooms comfortably and got already used to the little animals like mice, geckos or spiders, which shared the place with us. Soon we made friends with Grizzly, the dog, which accompanied us wherever we went and one of the cats, which was always hungry and enjoyed being caressed.

Our meals we had in a common dining room/kitchen, a place, which we liked from the first time we saw it. Like all the buildings in Thulir except our house, it was constructed in the traditional way of the region. We enjoyed the food, which was vegetarian and very varied. What we missed were salad, raw vegetables and fruits. It was not the season for local fruits and in the few shops there were no fruits to buy. So when we had the possibility to go to Harur or Salem, the next bigger towns, we bought a lot of pineapples and oranges. In Thulir, there are no forks or knives, so we had to learn to eat with our fingers.
But there are also other things, which first were strange, like the temple music, which could be heard all over the village during about two weeks and which we never liked, but sometime it gets just a part of the life here. Fortunately there were power cuts always again, the more we enjoyed then the perfect silence of the place.

In Sittilingi there is not much to buy, so you will also not find toilet paper. After six weeks we didn’t miss it at all, we even preferred the Indian way of going to the toilet.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/r2.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/r2.jpg)

Sittilingi lays in a valley in a beautiful rural landscape, which we liked very much. There are wonderful tamarind trees, a lot of palm trees and big bamboo forests. The people grow bananas, sugarcane and turmeric and all is surrounded by endless forests on the hills on both sides of the valley.
Sittilingi is a village like many others in this area. It has two parts in a distance of two kilometres. Between them there is a junction with two small bars or shops, where you can drink chai or get some food and here is also the bus stop. Next to this place there is a hospital lead by friends of Anu and Krishna.
The village is a mix of old traditional houses and new ones built of bricks and concrete.

There is nothing special in Sittilingi, but the longer you stay the more things are special, because they become familiar to you. By the time our life became a daily routine, we went to the village every day to buy milk or other things, we made walks or bicycle tours around the village or to Kottapatti, where there is a nice market on Sundays, we swam in the nearby river, we took our chai at the junction and when we travelled it was by bus. After a while, people knew us, and also what we were doing here. Most of them don’t speak any English like we don’t speak more than a few words of Tamil, so we talked with a smile and our hands.

[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/r3.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/r3.jpg)



People were always very friendly to us, they offered us a seat in the bus or even a lift on their bike and we remember well, when a coconut picker climbed on a palm tree once again, just to offer us a drink from a coconut. We liked it to watch people working, covering the roofs of their houses, ploughing fields, selling cooking pots, harvesting sugar cane, cooking idlis and much more. A special experience was my visit at the barber to get a haircut on a Sunday, like all the men in this area. Like anything else we did around the village, everybody knew, what we had done, before we could tell about it. On another Sunday Susan bought golden earrings in Kottapatti and on Monday morning all the women in Thulir wanted to see and to know the weight of them.
Anu and Krishna told us a lot about the life of people in this area. They live here since many years and know everybody. It was very interesting to hear about the special situation of the people, who are Adivasi (indigenous or tribal people), about school, religion, customs and festivals, about the building of their houses or about the changes also happening here. Together with all our own impressions, we found out much more about this part of India, than it would be possible just by travelling.
In our last week, Susan had the opportunity to accompany Anu visiting many families of the schoolchildren. These visits gave her an insight into the daily life of the families, into the houses and also into the problems of the people. Susan accompanied Anu and the young women to a two days handicraft workshop in Thiruvannamalai and I did the same with four of the young men, who participated in the Chennai marathon. Both of it was a very interesting possibility to see, how Indians deal with all the things you have to do when you are travelling like to buy a ticket, find a place in the bus or train, eating on the way, sleeping out and many others.
[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/r4.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/r4.jpg)


### Working in Thulir


It took us some time to find out, how the work in Thulir is organized, which works have to be done, which events take place regularly or that a group of four older students is responsible for most of what has to be done. So most of our questions we could put to them, we could also discuss our suggestions and ideas with them, what helped us, because one of them was always around.

In the beginning they were not trained in speaking English, so it was not easy to make clear appointments and plans. And even if they were clear, that didn’t mean, that they could not change overnight. This was surely one of our biggest challenges in Thulir and in India: People don’t plan and organize their life and work like we do.
Even if we didn’t understand all the reasons for that, by the time we learned to appreciate the advantages of not planning too much.

Susan first joined Florence in her classes to see, what was already happening and how she wanted to continue. Soon she decided to do English classes every morning with the older students, to teach English to the two nursery-school teachers and to work with the schoolchildren, who arrived in Thulir around 4pm, when they had finished normal school.

Teaching the older students was very interesting but also taxing, because they were highly motivated and often didn’t even want to finish the lessons. Their knowledge was quite different, so after a while we split them in two groups, one had English class, the other worked with me, after 90 minutes we changed. The main aim of the lessons was to teach the students talking and understanding. Their English was often made of words, which they put together. For example they said: “Anu ask”, when they did not know something or “house going”. When we asked “who? ” or “when?” they often did not understand or could not answer such questions. Susan spent a lot of time teaching them to make sentences like “You must ask Anu” or “Tomorrow I go to my house” and to answer questions like “Why do you go to your house?” “I work with my father”. All the students liked it very much to talk in the lessons and did not want to work in their books anymore. They made a lot of progress and more and more it was possible to have a good conversation with them. Very important for this progress were two facts: First, Susan and I don’t speak Tamil, so the students could not change to their language, when they did not understand or know how to say something. Second, we did not only have classes or worked together, but also spent a lot of free time together. We had our meals together, travelled, played or just talked whenever we met.
[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/r5.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/r5.jpg)
With the schoolchildren, Susan mostly interacted in small groups and played with them easy games to learn English. She taught the whole group some English songs and one in Swiss German, which was fun. Who and how many of the children came, changed from day to day and together with the fact that they speak very little English, this made it quite difficult to find out how to work with them or only get to know their names. But also this started slowly getting better, but would have taken more time.
On one of our last days in Thulir we organized a games day for all the schoolchildren. All the older students were responsible for one of the games and thanks to their help the day became a big success and everybody had a lot of fun.
Anu and a group of the older students was very interested in the Swiss school system and how teachers teach in Switzerland. They were eager to hear about Susan’s work and had lots of questions. They were also very astonished about the fact that public school in Switzerland is a good school.
In Switzerland I work as a teacher for handicraft. So it was obvious that I would teach working with wood or metal also in Thulir. There were three ideas, what I could do with the older students: One was to find out whether we could produce things that could be sold, for example bee-boxes, which had already been ordered by some farmers earlier. Another one was to show to this group, how they could teach these subjects and work with the batch of new students, who is supposed to start in Thulir next year. The third idea was to produce different things like small desks, a box for a computer or a rain cover for a water heater, things, which had been planned before but not yet realized.
In Thulir there is a workshop with a small room where the tools and materials are stored and a covered place in front of it. There are basic tools, mostly for working with wood and some stock of wood and metal. The whole place was clean and well cleared.
[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/r6.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/r6.jpg)

In the workshop there is only solar power, which is not strong enough to work with machines, so all work has to be done by hands. Therefore and because Sittilingi is surrounded by big forests, we thought to work mainly on wood. Soon we had to change our ideas, because it was not possible to buy local wood. Together with the students, I tried to get it at different places, but without success. It would take too much space to explain why it was not possible and I’m also not sure, whether I understood it at all. We decided to change to work on metal, which is cheap and easily available, but most of it had to be bought and transported from Harur, what takes a daytrip.
Near Thulir there is another workshop, where three young men are working with the support of Ramsubu, a friend of Anu and Krishna. There is more space there and one electrical output to work with a cutting and a drilling machine. There is also the equipment for arc welding and for the gas cutting of metal, so that the conditions for working on metal were better there.

But first we had to clear up the workshop. Half of the space could not be used, because it was full of all kind of things and materials. There was a lot of waste, but also still useful things, which we took to Thulir or stored them for later use.

For this purpose we built a rack, we fixed and sorted the tools, we made a simple installation for waste separation and cleared also the outside metal stock. These works kept us busy during the whole time, but we worked also in Thulir. We constructed a workbench with the possibility to fix wood or metal pieces to cut or work on them, we made metal feet for the computer desks, a new blackboard and many other things. Most of the time I worked with the seven young men, but by the time, I also tried to support the three young women, who are interested and skilful in making jewellery.

We enjoyed it very much to work with these young people and I’m sure, that I learned from them as much as they did from me/us. They are motivated to learn and have already many skills and I was often surprised how they resolve problems in a clever and simple way.
The longer we stayed in Thulir and worked with them, the more we learned to work and search solutions with the material, which was already there and had not to be brought from far away. We learned to live with the facts and to improvise, to change our ideas and plans easier or even not to make them. We hope that our students learned in the same way from our way of thinking or working.

I tried to show them, that it can be useful to plan what could be done, if there is no power again instead of waiting or to store all the rest material in a way, that it can be found, when you need it or that cleaning is not a “bad work”, but an important part of good work.

It is a big difference between watching the skilful people at their work, admiring the simple and clever tools and methods and trying to work your self here. Soon I realised how difficult and tiring this sometimes can be. But maybe just these experiences showed me more of India than long journeys. I travelled quite a lot to buy tools or material, with my students, with Ramsubu or Susan, by bus or as fellow passenger on Senthils bike, and I saw so many interesting, amazing and incredible things, that I forgot the efforts easily.


### Leaving Thulir


As much as working with our students we enjoyed the free time we passed together. We met them in the village or at the junction, we laughed and talked together after the evening meals and I played also some volleyball matches with them. We remember well how the young men cooked chicken for Florence’s and our farewell, how proud and happy they were, when we liked it. In Salem I had the possibility to buy spaghetti, which our students had never eaten before. We cooked this meal twice for lunch and all Thulir people liked it.
[![](http://www.thulir.org/wp/wp-content/uploads/2013/09/r7.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2013/09/r7.jpg)

We could go on and on telling of our life in Thulir. It was a great and unforgettable experience for us and after six weeks, it was hard to leave. But after a longer time it would just have got harder and we believe that six weeks is a good length to stay. It gives you enough time to find out, whether you feel well here and it is not too long, if you do not.The last two or three days it was difficult to organize some work, because our students told us always, that they had other work to do.

On our last afternoon we finally found out, what they had done all the time. They had made wonderful gifts for us.

Our last evening in Thulir we spent in Anu and Krishna’s house and enjoyed another delicious Indian meal, before it was time to say thank you and goodbye to all Thulir people, but especially to Anu and Krishna, who have given us the chance to live and work in this interesting and courageous project, in this beautiful place called Thulir.
