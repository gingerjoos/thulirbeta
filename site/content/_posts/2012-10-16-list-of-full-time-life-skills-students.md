---
author: nimda321
date: 2012-10-16 07:21:37+00:00
draft: false
title: 'List of full time Life skills students '
type: post
url: /wp/2012/10/list-of-full-time-life-skills-students/
categories:
- Alumni
---

**_Updated  Oct 2012_**

_2004- 2005._

1. Vediyappan- s/o kanagaraj, sittilingi
2. Satya, Devaraj, Malaithangi
_
Students of the Basic Technology Course in 2006-2007 _

3. Senthil.S s/o samikannu
4. Balamurugan – s/o kanagaraj
5. Mohan – s/o Chinnayyan, Sittilingi
6. Perumal s/o annamalai Nammangadu
7. Jairam , Nammangadu
8. Madhu , S Dhadampatti
9. Satish, s/o  Imayavalli, Sittilingi Thanda
_
The 2007-2008 batch_

10. Rajammal- d/o samikannu, Moolasittilingi
11. Devagi- d/o Sadayan, Moolasittilingi
12. Krishnan-  s/o Samikannu, Moolasittilingi
13. Sangeetha,  nammangadu
14. Sangeetha d/o S chellamuthu, Thennagar
15. Nirmala- Velanur
16. Daivanai- d/o Palani, Malkaithangi
17. Chinraj- s/o Palani, Malaithangi
18. Chitra- d/o Annathurai, Moolasittilingi
19. Arul- s/o ponnan, malaithangi
20. Ilavarasi  d/o Mani, Sittilingi
21. Priya-  d/o Kulandiayan, Sittilingi
22. Vignesh- s/o  Ramayee, Sittilingi
_
2008-2009_

23. Vinu ,s/o Kullan, Wayanad, Kerala
24. Ezhumalai s/o sadaiyan, Moolasittilingi
25. Govindammal- s/o Rajendran, Moolasittilingi

_2009-2010 batch _

26. Venkatachalapathy, s/o chinnasamy , moolasittilingi
27. Vijayakumar, s/o Thankgamani, Sittilingi
28. Sakthivel, s/o Manikkam,Moolasittilingi
29. Jayabal, S/o Mayilsamy, Moolasittilingi
30. Chinnadurai, s/o Ponnusamy, Sittilingi
31. Kumar, s/o Annamalai, Moolasittilingi
32. Theerthammal, D/o Krishnan, Moolasittilingi
33. Santhashivam, Ponnani village, Gudalur, Nilgiris
34. Srikanth, s/o Rajagopal, Ponnani, Gudalur, Nilgiris
35. Velayudam, Ponnani village, Gudalur, Nilgiris
_
2010 -2011 Batch_

36. Raja, Nammngadu village
37. Ramamurthy, s/o Raman, Nammangadu
38. Parameswaran, s/o Rajamanikkam, Moolasittilingi
39. Chandramathy, d/o Ramanan, Sittilingi
40. Sathyaraj, s/o Dhanabal, Sittilingi
_
2011-12__Batch_

41. Theerthagiri, s/o Kulandayan, Sittilingi
42. Prashanth, s/o Theerthan, Sittilingi
43. Ajith kumar, s/o Arumugam, Sittilingi
44. Raman, s/o Dharuman, Aruvangadu
45. Dhanabal, S/o Manikkam, MoolaSittilingi
46. Jagannathan, s/o Kullan, Moolasittilingi
47. Thirupathy, s/o Ponnuswamy, Sittilingi
48. Parthiban, s/o Jayashankar, Malaithangi
49. Eeswari, Koraiyar village
50. Rajathi, Malaithangi
_
2012-13 Batch_

51. Chidambaram, s/o Annamalai, Sittilingi
52. Vijayakumari, d/o Selvam, Sittilingi
53. Ambika, d/o Krishnan, Sittilingi
54. Lakshmi, d/o Arumugam, Sitilingi
55. Sasikala, w/o Shiva, Sittilingi
[19 female students  and 36 male students]


