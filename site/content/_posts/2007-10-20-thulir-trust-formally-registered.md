---
author: thulir
date: 2007-10-20 16:27:00+00:00
draft: false
title: Thulir Trust formally registered
type: post
url: /wp/2007/10/thulir-trust-formally-registered/
categories:
- Newsletters
---

 

[![](http://bp2.blogger.com/_qz2j5uas_y0/R75pW0178GI/AAAAAAAAAKY/puVBLGfv614/s320/homesept-oct07+004.jpg)
](http://bp2.blogger.com/_qz2j5uas_y0/R75pW0178GI/AAAAAAAAAKY/puVBLGfv614/s1600-h/homesept-oct07+004.jpg)

  


_**Registration of Thulir Trust**_

                         

Thulir Trust has been formally registered in October 2007. The three Trustees of Thulir Trust are Mr.Manoharan (Managing Trustee), Dr.Shylaja Devi and Ms.Rama Sastry.  


Based on the rich experience of working with adivasis in Gudalur for more than two decades, these individuals decided to start this Trust to support educational activities among the children of disadvantaged communities like adivasis.

Detailed plan for activities has been drawn up. It is our objective to establish  Education Resource Centres in remote adivasi areas, to supplement the education they receive from Government schools and to work with the children who drop out from schools.  


[![](http://bp1.blogger.com/_qz2j5uas_y0/R75qVk178KI/AAAAAAAAAK4/49H3ZEpMVaE/s320/homesept-oct07+041.jpg)
](http://bp1.blogger.com/_qz2j5uas_y0/R75qVk178KI/AAAAAAAAAK4/49H3ZEpMVaE/s1600-h/homesept-oct07+041.jpg)  


  
We hope that all our friends will continue to support us during the coming years as well and will help us reach our goals.
