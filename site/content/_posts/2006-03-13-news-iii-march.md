---
author: thulir
date: 2006-03-13 06:17:00+00:00
draft: false
title: News III March
type: post
url: /wp/2006/03/news-iii-march/
categories:
- Newsletters
---

[![](http://photos1.blogger.com/blogger/8103/1906/320/dscn0288.1.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/dscn0288.1.jpg)  
[![](http://photos1.blogger.com/blogger/8103/1906/320/dscn0289.0.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/dscn0289.0.jpg)  
[![](http://photos1.blogger.com/blogger/8103/1906/320/dscn0287.0.jpg)
](http://photos1.blogger.com/blogger/8103/1906/1600/dscn0287.0.jpg)  

    
    <br></br><br></br><br></br><br></br><br></br><br></br><span style="font-weight:bold;font-size:130%;"><br></br></span><span style="font-weight:bold;font-size:130%;"><br></br><br></br><span style="font-family:georgia;">Carolyn, Patti and Nick</span></span><span style="font-weight:bold;font-size:130%;">  </span><br></br><span style="font-size:100%;"><br></br><span style="font-family:georgia;">Carolyn and Patti kept their yearly date with Sittilingi and were here from mid Dec</span><br></br><span style="font-family:georgia;">to mid Feb. They brought more learning materials similar to the ones they brought</span><br></br><span style="font-family:georgia;">last year with them and it has been a valuable addition to Thulir's resources.</span><br></br><span style="font-family:georgia;">Their friend Nick too had come this year and he is also enthusiastic about clay work.</span><br></br><span style="font-family:georgia;">They held a session with the children, and we have lots more of material to bake in the oven!!</span></span>
