---
author: thulir
date: 2009-12-05 07:35:59+00:00
draft: false
title: 04 September 09
type: post
url: /wp/2009/12/september-09-diary-b-t-course/
categories:
- Life Skills Course Diary
---

**Building Construction Part 8 --Construction of the Cycle shed.**

Further progress was made this month on the completion of the walls and putting up of the roof of the cycle shed.

As the structure took shape there was excited anticipation of parking all the Thulir cycles. In the meantime, more students have aquired cycles, so the shed is just barely enough! Maybe its time to plan for the next shed /extension!

[![Construction](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept011.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept011.jpg)

[![Construction](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept012.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept012.jpg)

[![Construction](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept014.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept014.jpg)[![Construction](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept017.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept017.jpg)[![Construction](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept018.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept018.jpg)

**Electronics **

Sanjeev and Anita came for 10 days. They held electronics classes and covered a range of topics like resistors and their values; reading resistor values using colour codes; measuring values of resistors and transistors using a multimeter and building simple circuits in a bread board and taking measurements to understand what resistors/ transistors do in a circuit.

They also taught songs, games and helped with the farming work.

[![Electronics](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept006.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept006.jpg)

[![Electronics](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept010.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept010.jpg)

**Organic Farming**

Spurred on after Perumal attending a workshop on organic farming methods, work on raised bed vegetable planting and a couple of test plots for paddy cultivations using improved methods was taken up this month.
[![Organic Farming](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept004.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept015.jpg)

[![Organic Farming](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept008.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept015.jpg)

**Soap making**

Rajammal and Devagi continued their soap making sessions, demonstrating and teaching various groups of students.

[![Soap Making](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept015.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept015.jpg)

[![Soap Making](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept013.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2009/11/sept013.jpg)
