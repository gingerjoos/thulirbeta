---
author: thulir
date: 2009-09-03 17:26:52+00:00
draft: false
title: 03 August 09
type: post
url: /wp/2009/09/august-09-diary-bt-course/
categories:
- Life Skills Course Diary
---

![Balu and Balaji](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-481.jpg)




_Balu [left extreme] and Balaji [third from left] have made significant contributions to the course this month.  After three weeks of teaching Balu left this month. Balaji, an enthusiastic volunteer came again this month to help in the course. He plans to spend more time at Thulir in September._

**Building Construction Part 5 -- Time to test Stabilised Adobe Bricks**

Yogananda and Subhas Basu, both with vast experience in stabilised mud constructions [and from whom we got the idea of making stabilised adobe bricks], visited Thulir. This was an excellent opportunity for us to learn more. They showed us how we could test for flexural strength of the bricks in the field. From this we can estimate the likely compressive strength of the bricks. The following pictures show the testing process. The results were very encouraging.

![adobe testing 1](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-012.jpg)


![adobe testing 2](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-011.jpg)


![adobe testing 3](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-010.jpg)


![adobe testing 4](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-009.jpg)


**Building Construction Part 6 -- Serious wall building!! **

With the experience of the previous exercises, we decided to venture into a slightly big project. The wall construction for a training workshop was about to begin around first of August.  So we thought this is a good opportunity for the students to do some serious stabilised mud block masonry. Balu, one of the adivasi masons trained by us in Gudalur, agreed to come over for three weeks to train the students. Since Balu himself was trained under similar circumstances, his sessions turned out to be very good, with him gently, sympathetically and patiently explaining and demonstrating the nuances of good masonry. Over 10 days, the students were involved in raising 120 feet length of wall to 4 feet height.

![wall building 1](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-007.jpg)


![wall building 2](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-008.jpg)


![wall building 3](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-002.jpg)


![wall building 4](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-49.jpg)


**Cycle repair skills**
With so many of Thulir students using bicycles, there is always maintenance work to be done. Many students of Thulir use the tools in our workshop to fix their cycles. This month as a number of Thulir's cycles are in need of servicing, this has become a project in itself. From basic fixing of punctures, to complex replacement of parts and fixing wheel alignments a range of skills are being explored.

![cycle repair 1](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-004.jpg)


![cycle repair 2](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-005.jpg)


**Balaji visits again**

Balaji came back to Thulir to spend more time with the children. In this visit he was involved in taking classes on communication skills for the students. He also started training Vinu, Ezhumalai and Shaktivel for the Kauveri marathon run. He has made a [detailed report](http://www.ashanet.org/projects-new/documents/967/visit5to11Aug.pdf) on his visit.

Balaji also started sessions on computer basics. The idea is to enter data on attendance, temperature, rainfall records [which we maintain daily in Thulir], kitchen stocks and accounts and then learn to analyse the same.

![computers 1](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-50.jpg)


![computers 2](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-43.jpg)


**Bamboo Key Chain making**

One of the products made as part of craft work is the bamboo key chain. It is very popular with friends of Thulir. Team Asha from Bangalore asked for 400 key chains to be made for an Independence day event in Bangalore. Vinu took charge and got the students to pitch in and succeeded in meeting the tight deadline.

![key chains 1](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-003.jpg)


![key chains 2](http://www.thulir.org/wp/wp-content/uploads/2009/08/aug-013.jpg)


**Soap Making**

Soap making process is being taught this month. The soaps are handmade using a cold process. Rajammal and Devagi, now fairly well experienced in soap making, demonstrated the process to the whole batch. Subsequently, a different group of students makes soaps every week. 

![soap making 1](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-039.jpg)


![soap making 2](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-040.jpg)


**Bee Keeping**

The Bee keeping sessions continued; Two boxes were opened for cleaning and maintainanace and also one migrating colony was captured and shifted into an empty box.

![bee keeping 1](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-034.jpg)


![bee keeping 2](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-035.jpg)


![bee keeping 3](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-037.jpg)


**Building Construction Part 7 -- Continuing the Cycle shed construction!! **

After a small break, the cycle shed construction was taken up again and the rest of the wall was constructed using stabilised compressed mud blocks.

![shed 1](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-020.jpg)


![shed 2](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-021.jpg)


![shed 3](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-022.jpg)


**The excitement of building a masonry arch**

An arch was constructed by the students as part of the wall of the shed. The students learnt the concept behind building an arch and the forces involved. 

![arch 1](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-030.jpg)


![arch 2](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-028.jpg)


![arch 3](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-027.jpg)


![arch 4](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-026.jpg)


![arch 5](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-024.jpg)


![arch 6](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-023.jpg)


**Making a bamboo table lamp using white LEDs**

We got some orders for bamboo LED table lamps, and so we started making a prototype this month. After many trials we finally managed to put together one. We used an old discarded Harddisk as a base [We thought the weight would provide stability!]. In the next phase, students in pairs will make replicas of the prototype. Of course we don't have too many waste HDDs so we will have to improvise using bamboo!

![](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-51.jpg)


![Table lamp 2](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-52.jpg)


![Table lamp 3](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-017.jpg)


![Table lamp 4](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-018.jpg)


**Vegetable gardening**

The hope for rains still continues and with optimism, vegetable bed preparations and some planting work was done this month. We had three showers totalling 10 cm this month but somehow more rains have been eluding our area and we keep our fingers crossed. For the time being there is enough water for domestic needs but not enough for farming.

![Veg garden 1](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-031.jpg)


**Wall painting**

Vinu started painting Thulir walls using themes from Warli paintings. We thought the simple figures from this style would be easy for students to draw. Vinu has made a few sample drawings and has been taking classes on drawing using this style on paper first. In the second stage wall paintings by other students will be taken up.

![wall painting 1](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-46.jpg)


![wall painting 2](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-47.jpg)


![wall painting 3](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-42.jpg)


![wall painting 4](http://www.thulir.org/wp/wp-content/uploads/2009/09/aug-41.jpg)

