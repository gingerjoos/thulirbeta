---
author: nimda321
date: 2015-06-29 09:06:12+00:00
draft: false
title: Photo essay -- recent events at Thulir
type: post
url: /wp/2015/06/photo-essay-recent-events-at-thulir/
categories:
- Newsletters
---


[![](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010821.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010821.jpg)

    The new Thulir School was formally inaugurated on June 17th. Senthil, one of the earliest students at Thulir and now a senior staff, lights the lamp.




[caption id="attachment_2293" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010809.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010809.jpg) Parents, members of the education committee, and students  participated in this event.[/caption]




[caption id="attachment_2288" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010840.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010840.jpg) A discussion on the kind of education and the values that should underpin it, followed.[/caption]




[caption id="attachment_2296" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010856.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010856.jpg) Prof and Mrs Ravindran, Mrs and Mr Nagarajan have generously permitted their residences to be used as classrooms. Here you can see the bamboo work added to create a class room[/caption]




[caption id="attachment_2294" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20150618_112706236_HDR.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/IMG_20150618_112706236_HDR.jpg) children during a break[/caption]




[caption id="" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010782.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010782.jpg) New classroom![/caption]




[caption id="attachment_2291" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010743.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010743.jpg) Thulir Alumni Jayabal, Dhanbal, Kumar and Kumar, along with Thulir staff have been working hard to get the buildings ready.[/caption]




[caption id="attachment_2287" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010850.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/P1010850.jpg) The new Reception area under construction.[/caption]




[caption id="attachment_2305" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/SAM_3244.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/SAM_3244.jpg) Starting of the day![/caption]




[caption id="attachment_2303" align="aligncenter" width="267"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/SAM_3265-e1435318071836.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/SAM_3265-e1435318071836.jpg) Quiet time![/caption]




[caption id="attachment_2304" align="aligncenter" width="267"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/SAM_3285-e1435318043378.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/SAM_3285-e1435318043378.jpg) Activities![/caption]




[caption id="attachment_2301" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/SAM_3310.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/SAM_3310.jpg) Hmm, what shall I do now?[/caption]




[caption id="attachment_2297" align="aligncenter" width="400"][![](http://www.thulir.org/wp/wp-content/uploads/2015/06/SAM_3325.jpg)
](http://www.thulir.org/wp/wp-content/uploads/2015/06/SAM_3325.jpg) Fun outdoors.[/caption]






