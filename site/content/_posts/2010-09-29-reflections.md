---
author: thulir
date: 2010-09-29 12:28:22+00:00
draft: false
title: Reflections - 2010
type: post
url: /wp/2010/09/reflections/
categories:
- Reflections
---

On April 14th, the Tamil New year day, in 2004, we inaugurated the Thulir Learning Centre at Sittilingi. It is more than 6 years since then, and we thought it is a good time now to reflect back and share our journey so far.

In the initial years, there were many periods of frustration. We started our work with the 10-14 age group of mainly school going children. We started with after school classes in the evenings and also during the day on weekends. The appalling level of academic skills among the children seemed quite insurmountable. We found that the children in spite of having spent 5 years or more in school hadn't learnt basics and were more keen to get into rote learning to tackle exams. Though some of them could read, their ability to comprehend text was poor. We were keen to get their basic skills corrected. It was a struggle. We couldn't see where we were going. "Were we making any progress at all?" was a constant question in our minds. Our limitation in terms of Humans Resources, space, lack of adequate time to spend with children seemed huge.

Along with the school going children, we slowly started working with teenage children who had opted out of schools too. With them we started working full time.They were in a similar situation. In addition, having spent more years in school and having failed exams, they had a huge block towards reading and writing. But they also believed that passing the 10th public exam was an important qualification they ought to possess.

But now after 6 years we feel most positive. We can now see tangible improvements and growth in our students. Some of our students who have gone out, come back in the holidays and actually list the benefits they have got from spending time at Thulir. We realise now that enormous patience is needed for educators. And for doing education work! In the initial years we expected too much improvement in too short a time. It takes quite some time for something to take root, branch and flower in a person's life. It is so unlike building construction or health work, where results are visible in a comparatively short span of time ... a few days or months.

We notice that our students who have been coming to Thulir for 3 years or more are generally more confident, not afraid to talk to outsiders and are more aware of things happening around the world. These were children who were afraid to talk even to us, who thought their "country" was Tamil Nadu and asked whether America was a vegetable [“Ka' in Tamil is a suffix to most vegetables names]

The older children [who came to us full time] had a great fear of reading when they came to us. They now read the Tamil Newspaper regularly, but read other books only if they are required to do so. They do not pick up books to read for the sake of reading and enjoying. But the children who came to us from a younger age are enormously interested in books and want to read them.

Our work with the older age group children has been one of trying various different approaches. While academic class room sessions have been very frustrating [they all seem to have a well developed ability to automatically switch off the moment a concept becomes difficult], sessions that involved working with hands seemed to be more welcome. We started with simple tasks that needed to be done around in the campus -- from gardening to laying water pipelines to some electrical wiring work. We slowly expanded to doing masonry, some electronics work, bamboo craft etc. . The students showed enthusiasm and seemed comfortable. Of course, in a group, we also found that different individuals were enthusiastic about different types of tasks -- so someone showed interest / was a natural in Masonry while another was good at and enjoyed electronics, while someone else was happy in the garden and could grow plants seemingly effortlessly.

The students did not see this as education, and so we did not push them into very in depth and repetitive learning of skills, as normal vocational training is done.

For years we have been trying to strike a balance between academic sessions and hands on work sessions for this group of students. We want both flexibility to respond to each student's needs but also some organisation so that learning is smooth. It is difficult and we have been trying various combinations, and are yet to decide on the ideal.

There has been yet another important area of learning. This is the time we spend talking/ counselling/ discussing various issues we feel are important. Issues about teenage/ adolescence; about growing up and relationships with older people; relationships between genders; changes in the society and the technologies that come in; geography [different places and people]; politics; global issues such as global warming and climate change; taking interest in current events/ news; etc. , etc. .

We found that whatever be the kind of course combination that our students went through, their time in Thulir seemed to have given them self confidence. The result was often unexpected -- some of our students who we never thought would be able to handle academic work, went back on their own to the Schools and rejoined to finish public exams. [See appendix on Thulir Alumni].

_**---The question of ideology / methods of teaching: **_

We didn't follow any fixed ideologies in our work so far; we have been open to look at various methods of teaching to see what we could learn in terms of ideas or insights and have tried to use them wherever we felt it was appropriate. We believe that not getting bogged down by methods/ ideologies is important to achieve a sense of freedom for the learner and for creating a right atmosphere to learn. We believe in trying to respond to each child/ individuals specific needs. This is difficult and at times very frustrating; but in the long run rewarding.

_**---- Our own learning:**_

We have learnt enormously and grown personally in the last 6 years; and it has been a huge learning experience for us. We have learnt a great many important lessons from the children and the community. Their values of simplicity, ability to live with few material things and a low ecological footprint, modesty, a quiet dignity, ability to learn new things in-spite of great odds stacked against one, Knowledge of forests, plants, living beings, agriculture; total involvement in whatever one does, lack of ego., etc.... continue to amaze and inspire us. Living here and running the centre has meant that we have learnt to do many things required ourselves.So right from cleaning our spaces, toilets, etc...., to dealing with different kinds of people, animals, growing plants and trees to making small repairs to pipes, electrical equipment, teaching etc...., had to be learnt. Though we are not brilliant in any of these skills we have begun to learn quite a range of skills.

--- _**"working with hand " v/s "working with the head" **_

This is one of the most crucial question which we feel, needs to be addressed by any educator today.

When we started working with older children, we did introduce "hands on work"; but with caution. But over the years we found that they are extremely good, fast and skilful with their hands. They master each skill very fast and once they do that their self confidence grows enormously. This self confidence then spreads to other aspects of their lives - some even tackle academics which they found difficult earlier.Over the last 5 years we have watched around 5 teenagers flower this way. But the community around , though recognising that each of them have improved enormously, still views hands-on work like this as that meant only for failures..

Our mainstream idea of Education is one very partial to " working with the head". Also "working with Hand " has been reduced to very specific skills training in the so called "Vocational" mode. The very word evokes the idea of it being "education for failures". This of course is consistent with the idea that "white collared work" is more superior and desirable. To make matters worse we also have the historical baggage of our "Caste" system where castes that worked with their heads were at the top of the ladder.

Children from rural/ poor backgrounds are traumatised by their schooling -- facing bewildering new academic skills they need to pick up, in schools that have very bad quality teaching and where they are constantly reminded of how poor they are in these skills and abilities. Improving quality of teaching would definitely help. But it can be helped a lot more by providing opportunities in schools for working with hands [a skill they already have/ or find easy to acquire]. This would increase their confidence and help them perform better in acquiring more difficult academic skills. This would also help them in valuing skills they already possess and skills that society badly needs today [the industry --at all levels, be it our local mechanic shop or a multinational corporation --is constantly bemoaning the fact that we don't have skilled workers].

The reality of is that the hand cannot do any work without involving the head and the head does benefit/ grow with the work done by the hand. The challenge before us is how do we integrate the two as part of our education. This would enhance our overall quality of education for all sections -- children of urban, educated classes as well as children who are rural/ poor/ first generation learners. Urban children are not as skilful with their hands as rural children and so introducing this would serve as an equalising factor.

As compared to conventional “vocational” training, the Hands- on work in school level should provide variety of learning opportunities. It should also include real life problems [for instance fixing / repair of things at school and from the community/ homes around; craft work that produces utilitarian items] , apart from art/ craft work which may not be utilitarian. Just as we teach children in schools a range of academic subjects, we need to provide a similar range in “working with Hands” skills too. We find that even at the age of 16 or 17, students are not in a position to choose a vocation, particularly since they haven't experienced any of the vocations till then. This is another important reason for offering variety, so that they may realise for themselves what various skills involve and which skill/s one is comfortable practising/ has interest in/ or has a natural talent for.

This brings us to a larger question of what "work" means to us as individuals and to us as a society.

Is it merely a means to making a living? Can it be something more? Does it have anything to do with our individual happiness?

How much does the work we do for a living shape they way we look at ourselves, our value to society, our usefulness to our families, and so on. Obviously Society's prejudices play an important part. So should we go along with it? If society favours IT coolies to farm Coolies, should we encourage students to go along with it? Do we question this as educators?

We clearly see these concerns as the important issues to face now. The way we tackle these; think about these will shape our work in Thulir.

_**----Creating Communities: **_

We have always felt that our work was not only to raise the awareness or to educate people of the local community , but also to raise awareness about the grass roots in our urban middle-class community. We hope to create bridges, in order to build a community of sensitive, just, equitable, like minded, ecologically concerned community of people from all over the world. This is imperative now, given the globalisation of dominant forces – corporate interests, markets etc. .

We are happy that a great many people from various backgrounds and parts of the world have visited, interacted with Thulir, and the Thulir community has grown. A number of people have made significant changes in their lives, and lifestyles and we hope their interactions with Thulir helped them with this transition.

We had hoped for funding support from concerned individuals as against formal institutions. We have been lucky to have found friends even among institutions that support us. The support from Tribal Health Initiative, Asha Princeton Chapter, Asha Bangalore Chapter , Hunnarshaala, and have been crucial in our growth. They have been real partners in our effort and not merely funding agencies. Many Volunteers who came representing their groups have formed valuable friendships with the two of us and with many of the students of Thulir.

Now, we feel our students, after studying in Thulir for some time are in a position to create a community that can provide creative/ learning spaces for others -- children from the villages as well as adults from elsewhere around the world.

_**--- Our children, and their education: **_

We shifted to this remote, new place when our children were 11 and 6 yes old. We were under pressure from ourselves, our children and concerned friends and family with regard to their education. Though we were convinced that we could give them a good educational environment, there were anxious moments during the initial years. Were we being fair to our children?

We were in a totally new environment totally different climate with no house to live in, no organised work or funds. While we had to work on these aspects, we also had to engage our children in a positive, learning mode. They had to make new friends too. It was not easy.

Here too we avoided getting straight jacketed into a narrow definition of "home schooling" or "alternative schooling". Depending upon the age of our child/ and his current interests at that moment and learning needs; we organised variously home schooling, partially attending Thulir sessions, attending sessions at other alternative small schools for short periods [ranging from weeks to a full term], and joining a boarding school full time [at age 13 when we felt they had outgrown what home/ Thulir could offer them and that they needed to be in a school environment that can provide appropriate peer group/ intellectual stimulus ]

_**---- Working with the land:**_

One of the most satisfying aspect of life in Sittiligi the past 6 years has been the opportunity to work on the land and see it transform itself. When we purchased the land, it had been uncared for, for decades and was used as grazing land. The top soil had got washed away and the place was bereft of any trees. Even the shrubs were hard and thorny. The couple of Bamboo clumps at the edges had been cut and torched.

Slowly with building of bunds, digging pits and planting trees, mulching etc., the trees have taken root and grown, shrubs with flowers have come back to the land and now we have a rich fauna of birds, bees, insects, snakes and butterflies.

Also with Thulir student's increasing interest in organic farming, we are able to now begin growing crops and vegetables, in a small scale.

_**---- Using alternative technologies:**_

Moving to Sittilingi was also an opportunity for us to try to build a house using alternative, eco friendly technologies, and use alternative energy sources. Our house with a thatch roof and mud wall has been comfortable to live in, in terms of climatic conditions. We have been surprised at how comfortable it can be compared to conventional modern houses; both during peak summer when there is frequent power cuts and during winters when it can get cold in the nights/ early mornings.

Going Solar for electricity has meant giving up on the Refrigerator, fans in all the rooms, and washing machine. While this needed adjusting to [and sometimes frustrating], there have been positive changes – for eg., almost all our meals are freshly cooked!!, and we have discovered the taste of traditional ways of preserving leftovers – old rice soaked in water overnight and eaten with buttermilk and raw small onions is a family favourite!

_**------ The Challenges that face us:**_

We are faced with a traditional Adivasi Community in rapid transition. This has its repercussions often difficult to identify and address.

For example, we have increasing cases of suicides and the causes seem frivolous at first sight. Could there be deeper issues involved?

We can see increasing pressures of mainstream consumerist culture, concept of what constitutes basic needs is rapidly changing with ever increasing need for hard cash.

Increasing emphasis on acquiring degrees and the commercialisation of education and the exploitation this entails.

Farming as a way of life is fast disappearing in the village. This means a loss of traditional knowledge and seeds and even the idea of food self sufficiency at the local community level.

While the villager who has been a farmer should have the choice of changing his vocation/ migrating to the city; can we help those who would like to keep farming .against all the odds stacked against them?

It is not that we have all the answers; our job as we see it is to make people even aware of the situation and how it is affecting the community as a whole. To equip them to begin to analyse and understand what is happening around them and to them. The make independent decisions -- not based on any ideology but as a collective decision of common good and of individual freedom.

==============================================

_**Appendix A -- Marathon runnning and Cycling **_

Sridhar, an Asha Volunteer from Princeton, while visiting Thulir one weekend in 2007, wanted to go for a 8 km run as part of his marathon training. He wondered if someone can help him with the route he can take in Sittilingi. Senthil was given the task of showing him the way and it was suggested he bicycle with Sridhar for the distance. But when Sridhar ran, Senthil decided to run with him and so accompanied him, running barefoot. Sridhar was amazed at the ease with which Senthil could run long distance without any preparation and suggested that he take up training for marathons.

Following this, Santhosh a volunteer in Asha Bangalore , and who was training the Team Asha in Bangalore, helped us with training schedules. He came to Sittilingi with a group of volunteers who were training for a marathon, to do a long run in Sittilingi as part of their training. So our students go to interact with them and also get to observe marathon training and running. This was a good boost to our students, and gradually, more and more took to running. Santhosh has been coaching / training our students and starting with shorter distance [5 to 10km] runs, he has been able to gradually get them to do extend the distances. So we have students who have run full marathons, and a few who have run half marathons and many who have done 10 km runs.

Apart from the regular training in Sittilingi [which provides a healthy activity which doesn't require too much in terms of facilities[; these events have provided our students a very good opportunity to learn to travel to new places, meet new people and interact with them and to form friendships with Asha Volunteers. For Sittilingi children this is a great learning, given the fact that we are in a remote area, and they seldom get chances for such interactions.

_**Appendix B -- Thulir Alumni**_
[
Read this Report on what Thulir Students are doing after studying here, as on April 2010 >>](http://www.thulir.org/wp/2010/09/report-on-alumni/)
_As always, we would be happy to hear your comments and responses._


**********************************
