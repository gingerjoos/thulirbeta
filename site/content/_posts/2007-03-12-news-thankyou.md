---
author: thulir
date: 2007-03-12 03:14:00+00:00
draft: false
title: Thank you all for writing in.....
type: post
url: /wp/2007/03/news-thankyou/
categories:
- Newsletters
---

** **

Our last news update in this Blog has elicited such an overwhelming response.

We now know that so many of you read our Blog [even such a  long and  possibly rambling one as our previous blog !]. But we have been moved that so many of you have shown concern that we wrote about feeling low. We have been moved by your expression of concern; and by your sharing  of similar experiences [ we now know from you that these things happen and one can overcome with perseverance!].

Thanks to your writing in we dont  feel anymore that we are in some remote place facing challenges alone..... we are truly fortunate to have so many friends who care and are willing to express support. Thank you all so much.

We are also happy to say that we are feeling a lot more positive and optimistic. The Thulir children have played a major part by their positive feedbacks and by being responsive to our efforts of late.

We have had a hard look at Thulir and what we have been doing and have posted the thoughts as " Thulir -- some reflections". [One more post for you all to read :)].

Thank you all once again for all the support.

**************************
