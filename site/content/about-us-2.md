---
author: nimda321
date: 2011-06-09 12:02:00+00:00
draft: false
title: About us
type: page
url: /wp/about-us-2/
---

When we [link anuradha and krishna] moved to Sittilingi in 2003,  our idea was to create a space for learning that would be tailored to the local needs. We did a survey of the villages, and visited local schools to understand what was needed. To our surprise we found that almost all children below 14 were enrolled in schools! The parents were quite keen on schooling and so children were religiously sent to schools even when there were no teachers to speak of or any learning happening. We also found that most children dropped out of formal schooling at class 8 to 10 levels, often after failing exams. These teenagers consequently had very low self esteem, lacking in basic academic skills, frustrated and mostly migrated to nearby towns to work in the textile industry. Schooling had, however, convinced them that farming or any kind of work with the hands, is inferior and something to be ashamed of.

We decided to
a] try to improve academic skills of school going children  [link to Thulir programme for school going children]
b] try to see what can be done for the school drop out teenagers. [ link to Thulir full time Programme for Teen age students]

Over the years the after school programme has been providing a mix of academic skills [reading, writing , math] and other learning opportunities not available in school -- arts and crafts, theatre, interactions with visitors from cities/ other countries, filed trips, Environment studies etc. This has also been a training ground for some of our teenage students to learn to facilitate learning in younger children.

The full time programme for Teenage students has been exploratory in nature, given the complexities int he issues involved -- bad academic skills with an inability to work on exam preparations, expectations of parents in terms of passing public exams, a natural flair in working with the hands, but a sense of inadequacy/ low esteem in pursuing farming or any vocational skill. So over the years, we have slowly tried to teach a combination of academic skills with working with the hands. When the two are integrated with real life projects, there is much learning and increase in self confidence. In the initial years we started with electrical wiring, plumbing and electronics, as these were seen to be "cool" things to do by most students. Slowly we have added organic farming activities after much discussions, exposure trips etc. One important aspect of these projects is to learn basics of Business-- keeping track of costs and materials, working out estimates and profits etc. The idea is to provide possibilities of combining family farm work with supplementary income through small businesses. Most work that needs to be done in our campus [involving solar power, lighting, wiring , plumbing, farming etc.] and some in the THI hospital campus, and sometimes of other friends outside have provided opportunities for various projects.

An interesting development has been that the project based learning has given some of our students so much confidence and basic academic skills that many have been preparing and writing public  school examinations as private candidates. Some have  gone and joined  school to pursue further studies. We have been providing support for exam preparations by conducting special classes and providing time for self study. [add link "Thulir alumni"]

The other development is that most day to day functioning of Thulir is taken care of by teenage students who after their stint as students have joined the staff. These include teaching activities such as masonry, plumbing, wiring etc., organising evening sessions for school going children, running of the Thulir Mess [provides lunch to students and meals to visitors and staff, and snacks to evening children], keeping accounts of the Trust, etc.

We see this group becoming a small community that can help children of this valley in their Learning and set up small businesses which can become places for learning production and business skills.

Thulir has become a dynamic place...Ever changing and Evolving, reflecting the Educational process [acquiring knowledge through experience and exposure] that people in the institution undergo; while keeping in mind the community's needs.


---------------------------------------------------------------------------------------------------------------

Read Further:

Thulir life skills course [add link to page]

The course diary [add link to page] page documents a years happenings in the course.

Newsletters [add link to blog]
