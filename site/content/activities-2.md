---
author: thulir
date: 2011-06-10 05:42:08+00:00
draft: false
title: Activities
type: page
url: /wp/activities-2/
---

Activities in Thulir are designed to reflect our thinking of what Meaningful Education ought to be. This has to be a balanced mix of skills that involve

-- the "hands" : the ability to handle materials and make useful objects

-- the "head" : reading, writing, reasoning and critical thinking

-- the "heart" : aesthetic sensibility, sensitivity to the environment and community around us.

We believe that current Education makes a divide between the "Head" and the "Hands", classifying learning into Academic and Vocational skills.  Further ,  Education has come to mean excessive emphasis on "Academics" and hardly any on the "vocational". Matters related to "Heart", of course are "extra-curricular" activities that have no useful purpose! This has lead to a severe crisis in Education today.

We constantly dilute standards of academic learning to make qualifying for a vast majority of new and first generation learners easier; Simultaneously emphasising that vocational streams and farming are inferior career choices thereby denigrating all work by hand. Consequently we have vast majority of students graduating with poor academic skills and poor engineering skills.  Those who fail in this system are condemned to take on livelihoods based on hand skills and made to believe that they are lesser human beings for lack of so called "academic" skills. We are producing a vast population of frustrated youth, who have no avenues to dignified livelihoods.

slide show of activities -- crafts, class room, to projects.
