---
author: nimda321
date: 2011-09-23 12:24:07+00:00
draft: false
title: Getting to Sittilingi
type: page
url: /wp/getting-to-sittilingi/
---

_**IMPORTANT: Sittilingi is a small village and there are no lodges/boarding facilities in the village or in nearby villages. We have very limited accommodation and so please contact us in advance, if you plan a trip!!**_

Sittilingi is roughly equi-distant [about 80 kms] from Thiruvannamalai to its North East, Dharmapuri to its West, and Salem to its South West.

The nearest Village is Kottapatti to its North.





[View Larger Map](http://maps.google.co.in/maps?f=q&source=embed&hl=en&geocode=&q=Thulir,+Sittilingi&aq=&ie=UTF8&hq=Thulir,+Sittilingi&hnear=&radius=15000&ll=11.904979,78.615417&spn=0.806245,0.823975&z=9&iwloc=A)





## _The nearest Railway Stations are_:





	  * Morappur [55 kms]-- Trains to Chennai, Coimbatore and Mangalore stop here.
	  * Vazhapady [45 kms] -- Train to Chennai Egmore and Vriddhachalam junction.



## _By car from Chennai:_


The route is Thindivanam, Thiruvannamalai, Thandrampet, Thanipady, Naripalli, Kottapatty. [280 kms]

[See this Google map>>](http://maps.google.co.in/maps/ms?msa=0&msid=205551352201922401320.0004a25e49d128f770c34)


## _By car from Bangalore:_


The route is Hosur, Krishnagiri, Kariamangalam, Morappur, Harur, Theerthamalai, Bairanaikenpatty, Kottapatty. [220 Kms]

[See this Google map>>](http://maps.google.co.in/maps/ms?msa=0&msid=205551352201922401320.0004a25a80e20206ec512)


## _By bus from Chennai:_


Take a bus to Thiruvannamalai [about 4 hrs from Koyambedu].

The route from Thiruvannamalai to Sittilingi is Thiruvannamalai-Naripalli-Kottapatti-Sittilingi. There are only three direct buses from Thiruvannamalai to  Sittilingi. All these buses start from the bay where the Naripalli and Sathanur dam buses start. This bay is actually in the lane where all the buses come into the bus stand. Nobody in the bus stand will know about Sittilingi. So ask for Naripalli or Kottapatti.

Timings:
10:30 am - bus to Salem going via Kottapatti and Sittilingi. (This is the only Salem bus going via Sittilingi.)
12:50 pm - bus to Attur going via Sittilingi.
3:30 pm  - bus to Vazhapadi via Sittilingi.

If you miss these direct buses, take a bus to Naripalli, ask people there for buses to Sittilingi, and take another bus to Sittilingi. Be prepared to wait for some time, maybe even an hour, at Naripalli. At Sittilingi, get off either at the hospital or at a stop called Koravan Koil, ask people there for directions and either walk to Thulir or call if you want a pick up by bike.


## _By train from Chennai:_


Take Kovai Express upto Morappur. From there take buses to Harur and change to bus to AK Thanda at 10:30 am (this will reach Sittilingi at 12:15 pm). If you miss this bus there is a bus to Kottapatty at 11 am. You will reach Kottapatty at 12:15 pm and there is a connecting bus at 1 pm to Sittilingi.

The overnight train from Chennai Egmore to Salem is the other option. Get off at Vazhapady and there is a bus to Thiruvannamalai via Thumbal and Sittilingi at 5:45 am. This will reach Sittilingi at 7:15 am.


## _By Bus from Bangalore:_


Recently a direct bus from Hosur at 1:45 pm, has been introduced. This bus goes to a place called Aathur via Sittilingi. It will start from the bay where the Harur and Vellore buses start at the Hosur bus stand. This reaches Sittilingi around 7:15 pm.

There is also a direct bus from Krishnagiri. This leaves Krishnagiri at 10:30 am. It is a bus going to a place called “Kombai” via Harur, Kottapatti and Sittilingi. This starts from the bay where the Harur buses start. In fact the board on the bus will say Harur. This reaches Sittilingi at 2:45 pm.

Please note: No one in Hosur or Krishnagiri bus stands other than the conductor and driver of your bus will know of Sittilingi. So ask for Harur.

Alternative Route: from Bangalore take any bus going to Thiruvannamalai or Pondicherry and get off at Uthangarai and change a bus to Harur.

At Harur, you will get buses to Sittilingi at 5:15 am, 7:30 am, 10:30 am, 1:15 pm, 3:15 pm, 3:45 pm and 7:15 pm. In between there are buses to Kottapatty which is 10 kms from Sittilingi.



## _By Train from Bangalore:_


A convenient connection is to take the morning Ernakulam Inter city Express train from Bangalore [Dept 6:15 am from city] up to Dharmapuri. It reaches at 8:45 am there. At 9:20 am there is a direct bus to Kottapatty which will reach at 12:15 pm at Kottapatty. There is a bus to Sittilingi at 1 pm from there.



## _By Bus from Salem:_


There are only two direct buses to Sittilingi from Salem.

First bus at 10:20 am- a bus to Thiruvannamalai. Reaches Sittilingi at 12:45 pm. This is the only Thiruvannamalai bus from Salem going through Sittilingi. Be warned, the other Thiruvannamalai buses go through Harur.

Second bus at 4:35 pm- this goes up to Naripalli through Sittilingi.

In both these buses you can get off at a stop called Koravan Koil in Sittilingi and walk. Thulir is just down the road. Or call us if you want to be picked up by bike.

Please note: These buses start from the bay where the Karumandurai buses start and not where the other Thiruvannamalai buses start. Not many people in Salem bus stand will know of this bus or of Sittilingi. So ask for Thumbal or Karumandurai. Only the driver, conductor and other passengers in the bus will know of Sittilingi.

There are many other buses to Thumbal from Salem but connections from Thumbal to Sittilingi are few. You might have to wait for an hour or more.
